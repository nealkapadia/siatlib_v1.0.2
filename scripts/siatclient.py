import siat
import pandas as pd
import logging
from datetime import datetime



def main():
    #traindata = pd.read_csv("abc_train_data1.csv")
    #testdata = pd.read_csv("abc_test_data1.csv")
    traindata = pd.read_csv("MetricsDataSample.csv")
    testdata =  pd.read_csv("MetricsDataSample.csv")
    siatObj = siat.Siat()

    '''
    print("*" * 100)
    print("Running Fit")
    model_id = siatObj.fit(ThresholdEither=50, ThresholdBoth=30, AnalysisInterval=60000,
            CalcParamN=3, CalcParamM=3, CalcParamK=3, MetricData=traindata)
    print(f"Return value of fit is: {model_id}")
    print("*" * 100)
    print(f"Predicting for ModelID {model_id} derived from fit")
    analysis_file = siatObj.predict(MetricData=testdata,
            ModelID=model_id,ReturnType=3,
            ExcludeConstantlyBroken=1,ContinuousPeriod=300000,
            SamplingPoints=5,ThresholdConstantlyBroken =100)
    print(f"Predict results for ModelID {model_id} derived from fit returns {analysis_file}")
    print("*" * 100)

    old_id = 217504290695020705
    #old_id = 217504290695020669
    print(f"Predicting for an older ModelID {old_id}")
    analysis_file = siatObj.predict(MetricData=testdata,ModelID=old_id,ReturnType=1)
    print(f"Predict by passing an old Model ID returns {analysis_file}")
    print("*" * 100)

    print(f"Updating Model with ModelID {old_id}")
    updated_model_id = siatObj.update_model(ThresholdEither=70, 
            ThresholdBoth=50, AnalysisInterval=60000,
            CalcParamN=2, CalcParamM=1, CalcParamK=0,ModelID=old_id)
    print(f"Update Model Returns {updated_model_id}")
    '''
    print("*" * 100)
    print(f"Exporting Model : 217504290695020681")
    retval = siatObj.model_export(217504290695020681)
    print(f'Export Model returned {retval}')
    print("*" * 100)
    csv_file = '/root/pradshan/siat/scripts/results/217504290695020681_Model.csv'
    retval = siatObj.model_import(csv_file)
    print(f'Import Model returned {retval}')
    print("*" * 100)
    print(f"Exporting Model : Exporting the Converted Model : {retval}")
    retval = siatObj.model_export(retval)
    print(f'Exporting Converted Model returned {retval}')
    print("*" * 100)




#format='%(asctime)s:%(levelname)-8s: %(message)s'
#format='%(asctime)s:%(filename)s:%(funcName)s:%(lineno)d:%(levelname)-8s: %(message)s'
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        filename=f"./logs/restdebuglogs_{datetime.now().strftime('%Y_%m_%d_%H_%M_%S.logs')}",
                        filemode='w',
                        format='%(asctime)s:%(levelname)-8s: %(message)s')
    main()
