"""This module contains the utilities needed for siat

"""
import json
import requests
import datetime
from dateutil import tz
from pytz import timezone
import pprint
import time
import logging
import pandas as pd
import codecs
import os
import collections
import csv
import struct
import random
import zipfile
import chardet
import sys

import posixpath
import socket
import netifaces
class Utils:
    r"""Used to Interact with SIAT using REST APIs

    Parameters
    ----------
    None

    Attributes
    ----------
    input_params      : dict
                        It contains the dict from the input.params
                        json file, which contains all constants

    log               : logging.Logger
                        Has the Logger which is used for dumping log info

    """

    def __init__(self,file_name):
        self.log = logging.getLogger(__name__)
        self._read_params(file_name)
        self._trim_urls()
        self._create_lookup_dict()

    def _read_params(self, file_name):
        r"""Reads the input.params file and converts it into a dict
        
        Parameters
        ----------
        file_name  :  string
                      Contains the name of the json file which contains the 
                      system constants.

        Raises
        ------
        FileNotFoundError
            Raised if File is NOT found

        """
        try:
            with open(file_name) as json_file:
                self.input_params = json.load(json_file)
        except Exception as ex:
            self.log.error("File NOT Found", exc_info=True)
            self._create_params(file_name)
            self._read_params(file_name)

    def _create_params(self, file_name):
        print(f'{file_name} not found')
        print(f'Creating Params file')

        
        user_input = dict()
        user_input["server-details"] = dict()
        server = input("Please Enter the Server FQDN/URL for REST: ")
        port = input("Please Enter the Port Number For REST: ")
        user = input("Please Enter the SIAT Create UserName: ")
        import_file = input("Please Enter the SIAT import file location: ")
        export = input("Please Enter the location where the results need to be exported: ")
        temp = input("Please Enter a Temp location where the results will be processed: ")
        self.server=server
        if self._distenv_check()==0:            
            self.log.info(f'Centralized environment observed', exc_info=True)
            server_loc=temp
            dist_type=0
        elif self._distenv_check()==1:
            self.log.info(f'Distributed Environment observed', exc_info=True)
            server_loc= input("Distributed environment observed, Please Enter temp location of shared location-path on the server: ")
            dist_type=1
        else:
            self.log.info(f'Unable to verify client environment', exc_info=True)
            server_loc= input("Incase of centralized environment leave this field blank. Otherwise Please Enter temp location of shared location-path on the server: ")
            if server_loc=='' or server_loc==temp:
                server_loc=temp
                dist_type=0
            else:
                dist_type=1


        #Creating the DICT
        user_input["server-details"]["server"] = server
        user_input["server-details"]["port"] = port
        user_input["server-details"]["createuser"] = user

        if dist_type==0:                        
            user_input["storage-loc"] = dict()
            user_input["storage-loc"]["server-import-file-loc"] = import_file
            user_input["storage-loc"]["server-export-results"] = export
            user_input["storage-loc"]["server-tmp-loc"] = temp
            user_input["storage-loc"]["server-tmp-loc"] = temp            
            user_input["env-detail"] = dict()
            user_input["env-detail"]["arch-type"] = "0"
        else:
            user_input["storage-loc"] = dict()
            user_input["storage-loc"]["client-import-file-loc"] = import_file
            user_input["storage-loc"]["client-export-results"] = export
            user_input["storage-loc"]["client-tmp-loc"] = temp
            user_input["storage-loc"]["server-tmp-loc"] = server_loc
            user_input["env-detail"] = dict()            
            user_input["env-detail"]["arch-type"] = "1"

        user_input["urls"] = dict()
        user_input["urls"]["base-url"] = "v1/plantmgrapi"
        user_input["urls"]["incident-url"] = "incident"
        user_input["urls"]["model-url"] = "models"
        user_input["urls"]["model-export-url"] = "models/export"
        user_input["urls"]["model-import-url"] = "models/import"
        user_input["urls"]["model-data-url"] = "models/data"
        user_input["urls"]["metric-url"] = "metrics"
        user_input["urls"]["group-url"] = "groupinfo"
        user_input["urls"]["status-url"] = "status"
        user_input["urls"]["analysis-url"] = "analyses"
        user_input["urls"]["import-metric-url"] = "measures"
        user_input["urls"]["requests-url"] = "requests"
        user_input["urls"]["anomaly-export-url"] = "results/export"


        write_str = json.dumps(user_input, indent=4)
        #Dump the JSON into the param file
        with open(file_name, 'w') as fp:
            fp.write(write_str)
        self.log.info(f'Params file {file_name} created successfully. You can edit'
                      f' it anytime manually.')

    def _distenv_check(self):

        '''
        Checks if the environment will be same or distributed

        Parameters
        ----------
        siat_server : string
                        server entered by user, where siat is installed

        Raises
        ------
        SocketException:
            if socket is unable to get hostname or client ip

        Returns
        -------
            0   :   int
                    if client-server in same environment/machine
            1   :   int
                    if environment is Distributed
            -1  :   int
                    if unable to get client IP or hostname
        '''
        distval=-1

        try:
            hostname='http://'+socket.gethostname()
            local_ip=[]
            for interface in netifaces.interfaces():
                try:
                    for link in netifaces.ifaddresses(interface)[netifaces.AF_INET]:         
                        local_ip.append('http://'+link['addr'])
                except KeyError:
                    self.log.error("Key Error received while collecting local IP address", exc_info=True)
                    continue
            self.log.info(f"local_ip have been collected, local_ip are: {local_ip}", exc_info=True)
            if self.server in local_ip or self.server==hostname:
                self.log.info("SIAT and client ip matched, client and server are in same environment", exc_info=True)
                distval=0
            else:
                self.log.info("SIAT and client ip did not matched, client and server are in distributed environment", exc_info=True)                
                distval=1                
        except:
                self.log.error("Unable to get the client hostname or IP address ", exc_info=True)
                distval=-1
        return distval
 

    def _trim_urls(self):
        r"""Trims any unwanted //// from the url
        
        Raises
        ------
        KeyError
            Raised if key is not found in the params

        """
        param_dict = self.input_params
        try:
            for key in param_dict['urls']:
                param_dict['urls'][key] = param_dict['urls'][key].strip("/")
            self.input_params = param_dict
        except KeyError as ex:
            self.log.error("Key NOT Found", exc_info=True)
            raise ex
    
    def build_url(self, key):
        r"""Builds the URL needed for a task


        Parameters
        ----------
        key      : string
                   The key name to get the value from server-details input params

        Raises
        ------
        KeyError
            Raised if key is not found in the params
         
        Returns
        -------
        restful_url   :   string
                          The URL needed to do a Resful Task
        """
        param = self.input_params
        try:
            restful_url = f"{param['server-details']['server']}:{param['server-details']['port']}/{param['urls']['base-url']}/{param['urls'][key]}"
        except KeyError as ex:
            self.log.error("Key NOT Found while building URL", exc_info=True)
            raise ex
        return restful_url
    
    def send_request(self, url, req_type, post_dict = {}):
        r"""Sends a POST / GET request to the SIAT

        Parameters
        ----------
        url        :   string
                       POST or GET URL

        req_type   :   string
                       Either 'post' or 'get'

        post_dict  :   dict
                       If doing a POST request it must have the json input

        Returns
        -------
        response  :   string
                      Contains the response string from SIAT
        """
        self.log.info(f'Sending {req_type.upper()} request to SIAT')
        self.log.info(f'URL: {url}')
        pp = pprint.PrettyPrinter(indent=4)
        if req_type == 'get':
            response = requests.get(url)
        elif req_type == 'put':
            pass
        elif req_type == 'post':
            self.log.debug(f'Posting the following request to SIAT:\n')
            self.log.debug(pp.pformat(post_dict))
            response = requests.post(url=url, json=post_dict)
        self.log.debug(f'Received the following response for the above request:\n')

        try:
            self.log.debug(pp.pformat(response.json()))
        except:
            self.log.error("Looks like No Content in the Response", exc_info=True)
    
        return response

    def _create_lookup_dict(self):

        accepted = 'Accepted'
        p_error = 'Parameter Error'
        c_error = 'Communication Error'
        i_error = 'Internal Error'
        success = 'Successful Completion'

        lookup_dict = dict()
        lookup_dict['ModelCreate'] = dict()
        lookup_dict['ModelCreate']['202'] = dict()
        lookup_dict['ModelCreate']['200'] = dict()

        lookup_dict['ModelCreate']['202'] = {1: accepted,
                                             2: p_error,
                                             3: c_error,
                                             5: 'Specified Metric data Period is Duplicated',
                                             7: 'An error when Specifying modeling time',
                                             255: i_error}
        lookup_dict['ModelCreate']['200'] = {0: success,
                                             1: 'Job Suspension',
                                             3: c_error,
                                             4: 'Modeling Metric does Not exist',
                                             5: 'Metric Data in specified period does not exist',
                                             6: 'Analysis engine error',
                                             12: 'No valid invariant',
                                             20: 'Aggregation is unavailable because of memory restriction',
                                             21: 'As a result of aggregation, all metrics are invalid',
                                             251: 'Accumulation ID does not exist',
                                             252: 'Internal error of Accumulation Server',
                                             253: 'DB error of Accumulation Server'}

        lookup_dict['ExportModel'] = dict()
        lookup_dict['ExportModel']['202'] = dict()
        lookup_dict['ExportModel']['200'] = dict()

        lookup_dict['ExportModel']['202'] = {1: accepted,
                                             2: p_error,
                                             4: 'Specified model ID not found',
                                             6: 'Unable to Export',
                                             255: i_error}

        lookup_dict['ExportModel']['200'] = {0: success,
                                             1: 'Successful Cancellation',
                                             8: 'File write error',
                                             255: i_error}


        lookup_dict['ImportMetric'] = dict()
        lookup_dict['ImportMetric']['202'] = dict()
        lookup_dict['ImportMetric']['200'] = dict()

        lookup_dict['ImportMetric']['200'] = {0: success,
                                             1: 'Successful Cancellation',
                                             3: c_error,
                                             6: 'Invalid File format',
                                             7: 'File Read error',
                                             8: 'File Write error',
                                             9: 'No line to import',
                                             16: 'Lack of free space on disk',
                                             255: i_error}

        lookup_dict['ImportMetric']['202'] = lookup_dict['ImportMetric']['200']

        lookup_dict['ImportModel'] = lookup_dict['ImportMetric']
                                             

        lookup_dict['ModelData'] = dict()
        lookup_dict['ModelData'] = {1: success,
                                    2: p_error,
                                    3: c_error,
                                    4: 'Specified model ID not found',
                                    5: 'Model is not created',
                                    6: 'Start position is out of range',
                                    255: i_error}

        lookup_dict['RegisterGroup'] = dict()
        lookup_dict['RegisterGroup'] = {1: success,
                                        2: p_error,
                                        10: 'Duplication error',
                                        255: i_error}
        lookup_dict['GetGroupID'] = dict()
        lookup_dict['GetGroupID'] = {1: success,
                                     2: p_error,
                                     3: c_error,
                                     4: 'Specified group id not found',
                                     255: i_error}

        lookup_dict['RegisterMetric'] = dict()
        lookup_dict['RegisterMetric']['202'] = dict()
        lookup_dict['RegisterMetric']['200'] = dict()

        lookup_dict['RegisterMetric']['202'] = {1: accepted,
                                                2: p_error,
                                                3: c_error,
                                                255: i_error}
        lookup_dict['RegisterMetric']['200'] = {0: success,
                                                1: 'Job Cancellation',
                                                2: p_error,
                                                3: 'Duplicate metric names error',
                                                5: 'Specified Group ID not found',
                                                255: i_error}
        lookup_dict['ReqAnalysis'] = dict()
        lookup_dict['ReqAnalysis']['202'] = dict()
        lookup_dict['ReqAnalysis']['200'] = dict()

        lookup_dict['ReqAnalysis']['202'] = {1: accepted,
                                             2: p_error,
                                             3: c_error,
                                             5: 'A model ID which cannot be specified for analysis has been specified',
                                             255: i_error}
        lookup_dict['ReqAnalysis']['200'] = {0: success,
                                             1: 'Job Suspension',
                                             3: c_error,
                                             5: 'There is no metric data in the specified period',
                                             6: 'Analysis engine error',
                                             8: 'File write error',
                                             9: 'There is no metric data specified in the model',
                                             20: 'Metric data processing error because of memory restriction',
                                             251: 'Accumulation ID not found',
                                             252: 'Internal error of Accumulation Server',
                                             253: 'DB error of Accumulation Server',
                                             254: 'Communication error of Accumulation Server',
                                             255: i_error}

        lookup_dict['ExportAnalysis'] = dict()
        lookup_dict['ExportAnalysis']['202'] = dict()
        lookup_dict['ExportAnalysis']['200'] = dict()

        lookup_dict['ExportAnalysis']['202'] = {1: accepted,
                                                2: p_error,
                                                4: 'Specified Analysis ID not found',
                                                6: 'Unable to export a model',
                                                255: i_error}
        lookup_dict['ExportAnalysis']['200'] = {0: success,
                                                1: 'Cancellation',
                                                8: 'File write error',
                                                9: 'Analysis result not found',
                                                16: 'Insufficient space error',
                                                255: i_error}

        self.lookup_dict = lookup_dict


    def _lookup_response(self, response, req_type, http_type=''):

        try:
            res_code = response.json()['ResponseCode']
        except:
            self.log.error('Lookup Dict Not found')
            return

        if http_type=='':
            try:
                response_str = self.lookup_dict[req_type][res_code]
            except:
                response_str = response_code
        else:
            try:
                response_str = self.lookup_dict[req_type][http_type][res_code]
            except:
                response_str = res_code

        self.log.debug(f'{req_type} got a response code "{response_str}"')
        return

        
    
    def check_http_status_code(self, response, expected_status):
        r"""Check for the HTTP Status code from the SIAT response

        Parameters
        ----------
        response         :    string
                              Contains the Response string from SIAT

        expected_status  :    int
                              Contains what HTTP status is expected as response

        Returns
        -------
        1  :   On Sucess
        0  :   On Failure
        """
        if response.status_code == expected_status:
            self.log.info(f'Expected Behavior: Expected HTTP Status Code is {expected_status} and got the same')
            return 1
        else:
            self.log.error(f'Unexpected Behavior: Expected {expected_status} as HTTP Status Code but instead got {response.status_code}')
            return 0

    def check_response_code(self, response, req_type, expected_res, key=""):
        r"""Checks for the SIAT Response Code

        Parameters
        ----------
        response     :   string
                         Contains the Response string from SIAT

        req_type     :   string
                         'sync' or 'async' are the accepted params
                         If it was a synchronous request pass 'sync'
                         If it was an asynchronous request pass 'async'

        expected_res :   int
                         Expected Response Code from SIAT for the request

        key          :   string
                         Pass the key to look for in the Response Code from
                         SIAT

        Returns
        -------
        1  :   On Success
        0  :   On Failure
        """
        retval = 0
        try:
            res_code = response.json()['ResponseCode']
        except:
            self.log.error("Looks like Response is empty", exc_info=True)

        http_status_code = response.status_code

        if (req_type == 'sync'):
            if (res_code == 1):
                self.log.info("Expected Response Code is Successful Completion (1) and got the same")
                retval = 1
            else:
                self.log.error("Expected Response Code is Successful Completion (1) but got something else")
                retval = 0
        elif (req_type == 'async'):
            if (res_code == expected_res):
                self.log.info(f'Expected Response Code is {expected_res} and got the same')
                retval=1
                #Get the ID so that you can poll for 200 OK now.
                try:
                    self.idtocheck = response.json()[key]
                except:
                    self.log.error("Looks like Response is empty", exc_info=True)
            else:
                self.log.error(f'Expected Response Code is {expected_res} but got {res_code}')
                retval = 0
        else:
            pass
        return retval

    def _without_keys(self, d, keys):
        r"""Removes the specified key from the dict

        Parameters
        ----------
        d    :   dict
                 dictionary whose key needs to be removed

        keys :   list
                 list of keys that needs to be removed

        Returns
        -------
        new_dict  : dict
                    A new dict whose keys has been removed
        """
        return {x: d[x] for x in d if x not in keys}

    def convert_to_epoch_time(self, time, server_tzone):
       r"""Convert a date string to unix EPOCH Time


       Parameters
       ----------
       time           :  str
                         date string to be converted
       server_tzone   :  str
                         TimeZone to which you want the time to be converted to
                         other than UTC

       Returns
       -------
       time_tuple   :  tuple
                       Tuple with first element being UTC timezone
                       and second element being local server timezone
                       in milliseconds
       """
       input_params = self.input_params
       to_zone = tz.gettz(server_tzone)
       str_time = datetime.datetime.strptime(time, '%Y/%m/%d %H:%M:%S.%f')
       start_time_lst = [str_time.year, str_time.month, str_time.day, str_time.hour,\
               str_time.minute, str_time.second, str_time.microsecond]
       epoch_time_utc = datetime.datetime(*start_time_lst, tzinfo=datetime.timezone.utc).timestamp()
       epoch_time_server_tzone = datetime.datetime(*start_time_lst, tzinfo=to_zone).timestamp()
       return ((int(epoch_time_utc * 1000), int(epoch_time_server_tzone * 1000)))

    def get_time_for_tzone(self, server_tzone):
       r"""Get the current time as per the timezone

       Parameters
       ----------
       server_tzone    : str
                         TimeZone in which you want the current time

       Returns
       -------
       time            : str
                         String Format Time ('%Y/%m/%d %H-%M.%S %Z%z')
       """
       s_tzone = timezone(server_tzone)
       current_time = datetime.datetime.now(s_tzone)
       str_time = current_time.strftime('%Y/%m/%d %H-%M.%S %Z%z')
       return str_time
   

    def wait_for_request_to_complete(self, req_id, task):
        r'''Send the Get request to check on the status of the request id.

        Parameters
        ----------
        req_id   :  str
                    The Request ID or Model ID or Analysis ID which needs to
                    be checked to get the status of POST request.

        task     :  str
                    The Task which is being accomplished by this method.

        Returns
        -------
        1 : int
            Returns 1 if the response code is expected one
        0 : int
            Returns 0 if the response code is unexpected.

        Notes
        -----
        **Here we wait for 200 ok. If the request is in progress then 
        the status_code will be 202 accepted. If the request is completed
        at the time of the get request then we will get 200 ok. Check for
        response code when we get the 200 OK and see if the response code
        is Non Zero.**

        '''
        req_url = f"{self.build_url(key='requests-url')}/{req_id}"

        self.log.info(f'Checking the status of Async request of {task} task')

        while True:
            res = self.send_request(url=req_url, req_type='get')
            status_code = res.status_code
            self.log.debug(status_code)
            if status_code == requests.codes.ok:
                try:
                    res_code = res.json()["ResponseCode"]
                except:
                    self.log.error("Looks like Response is empty", exc_info=True)
                if res_code != 0:
                    #self.log.error(f'Unexpected Response Code for {task}', exc_info=True)
                    #raise Exception(f'{task}, response code: {res_code}');
                    self.log.error(f'Unexpected Response Code for {task}, response code:{res_code}')
                    self._lookup_response(res, req_type = task, http_type = '200')
                    return 0
                return 1
            else:
                self._lookup_response(res, req_type = task, http_type = '202')
            time.sleep(1)

    def _create_metric_dict(self, group_id):
        req_url = self.build_url(key='metric-url')
        req_url = f'{req_url}?mode=2&groupid={group_id}'
        res = self.send_request(url=req_url, req_type='get')
        metric_list = res.json()["PlantMgrMetricInfo"]
        name_id_dict = dict()
        for met in metric_list:
            id_list = [0]*2;
            id = met["MetricID"]
            grpCnt = 0
            if int(met["GroupCount"]) > 0:
                 grpCnt = 1
            id_list[0] = id
            id_list[1] = grpCnt
            name_id_dict[met["MetricName"]] = id_list
        return name_id_dict
 
    def _create_metric_group_dict(self):
        req_url = self.build_url(key='group-url')
        res = self.send_request(url=req_url, req_type='get')
        group_list = res.json()["PlantMgrGroupInfo"]
        name_id_dict = dict()
        for grp in group_list:
            id = grp["GroupID"]
            name_id_dict[grp["GroupName"]] = id
        return name_id_dict

    def _get_group_id_from_name(self, name):
        req_url = self.build_url(key='group-url')
        req_url = f'{req_url}?groupname={name}'
        res = self.send_request(url=req_url, req_type='get')
        group_id = res.json()['PlantMgrGroupInfo'][0]['GroupID']
        return group_id


    def _create_model_dat_info(self, file_name):
        #First line is the comment which has the model and dat info
        with open(file_name) as f:
            first_line = f.readline().strip()

        info_list = first_line.split(',')
        model_info_list = info_list[:-8]
        model_info_list[0] = model_info_list[0].lstrip('#')
        dat_info_list = info_list[-8:]
        model_dict = {'ModelInfo' : {'MeasureInfo' : []}}
        dat_dict = {'Dat_Info': {}}
        #Model Info Dict
        for x in model_info_list:
            element = x.split(':')
            key = element[0].strip()
            value = element[1].strip()

            if value == 'None':
                value = ''
            if key == 'MeasureInfo':
                element[2] = ','.join(element[2].split(';'))
                element[0] = "'MeasureInfo'"
                element = ':'.join(element)
                element = eval('{' + element + '}')
                model_dict['ModelInfo']['MeasureInfo'] = element[key]
            else:
                model_dict['ModelInfo'][key] = value
        #Dat Info Dict
        for x in dat_info_list:
            element = x.split(':')
            key = element[0]
            value = element[1]
            dat_dict['Dat_Info'][key] = value

        xml_dict = {**model_dict, **dat_dict}
        return xml_dict

    def mdl_to_csv(self, model_id, base_dir):
        r"""Converts the Binary MDL file to CSV

        Parameters
        ----------
        model_id : int
                   Model ID of the Exported Model

        base_dir : str
                   Directory in which the Model Directory was created


        Returns
        -------
        None

        """
        #Read the Model_def.dat file
        csv_file = os.path.join(base_dir, str(model_id), f'{model_id}_Model.csv')        
        csv_dict = dict()
        with codecs.open(os.path.join(base_dir, str(model_id), f'{model_id}_def.dat'), "r", "utf-16-le") as f:            
            for line in f:
                if line.startswith("FileVersion"):
                    fileVersion = str(int(line[line.index("=")+1:]))
                    csv_dict['FileVersion'] = fileVersion
                elif line.startswith("ModelName"):
                    modelName = line[line.index("=")+1:].strip()
                elif line.startswith("ModelType"):
                    modelType = str(int(line[line.index("=")+1:]))
                elif line.startswith("ModelCreateType"):
                    modelCreateType = str(int(line[line.index("=")+1:]))
                elif line.startswith("BaseModelID"):
                    baseModelID = str(int(line[line.index("=")+1:]))
                    csv_dict['BaseModelID'] = baseModelID
                elif line.startswith("ModelLearnType"):
                    modelLearnType = str(int(line[line.index("=")+1:]))
                    csv_dict['ModelLearnType'] = modelLearnType
                elif line.startswith("LearningType"):
                    learningType = str(int(line[line.index("=")+1:]))
                    csv_dict['LearningType'] = learningType
                elif line.startswith("CreateTiming"):
                    modelCreateTiming = str(int(line[line.index("=")+1:]))
                elif line.startswith("CreateTimeUTC"):
                    modelCreateTimeUTC = str(int(line[line.index("=")+1:]))
                elif line.startswith("CreateTime"):
                    modelCreateTime = line[line.index("=")+1:].strip()
                elif line.startswith("Comment"):
                    modelComment = line[line.index("=")+1:].strip()
                elif line.startswith("CreateUserName"):
                    createUserName = line[line.index("=")+1:].strip()
                elif line.startswith("ThresholdEither"):
                    thresholdEither = str(int(line[line.index("=")+1:]) / 100.0)
                    csv_dict['ThresholdEither'] = thresholdEither
                elif line.startswith("ThresholdBoth"):
                    thresholdBoth = str(int(line[line.index("=")+1:]) / 100.0)
                    csv_dict['ThresholdBoth'] = thresholdBoth
                elif line.startswith("AnalysisInterval"):
                    analysisInterval = str(int(line[line.index("=")+1:]))
                elif line.startswith("SignificantFigures"):
                    significantFigures = str(int(line[line.index("=")+1:]))
                    csv_dict['SignificantFigures'] = significantFigures
                elif line.startswith("NegativeInvariantsHolds"):
                    negativeInvariantsHolds = str(int(line[line.index("=")+1:]))
                    csv_dict['NegativeInvariantsHolds'] = negativeInvariantsHolds
                elif line.startswith("CalcParamN"):
                    calcParamN = str(int(line[line.index("=")+1:]))
                    csv_dict['CalcParamN'] = calcParamN
                elif line.startswith("CalcParamM"):
                    calcParamM = str(int(line[line.index("=")+1:]))
                    csv_dict['CalcParamM'] = calcParamM
                elif line.startswith("CalcParamK"):
                    calcParamK = str(int(line[line.index("=")+1:]))
                    csv_dict['CalcParamK'] = calcParamK
                elif line.startswith("Severity"):
                    severity = str(int(line[line.index("=")+1:]))
                    csv_dict['Severity'] = severity
                elif line.startswith("ExcludeAutoRegressive"):
                    excludeAutoRegressive = str(int(line[line.index("=")+1:]))
                    csv_dict['ExcludeAutoRegressive'] = excludeAutoRegressive
                elif line.startswith("FitnessThreshold"):
                    fitnessThreshold = str(int(line[line.index("=")+1:]))
                    csv_dict['FitnessThreshold'] = fitnessThreshold
                elif line.startswith("AutoRegressiveRange"):
                    autoRegressiveRange = str(int(line[line.index("=")+1:]))
                    csv_dict['AutoRegressiveRange'] = autoRegressiveRange
                elif line.startswith("AutoAnalysisUpdateFlag"):
                    autoAnalysisUpdateFlag = str(int(line[line.index("=")+1:]))
                    csv_dict['AutoAnalysisUpdateFlag'] = autoAnalysisUpdateFlag
                elif line.startswith("TotalStartTimeUTC"):
                    totalStartTimeUTC = str(int(line[line.index("=")+1:]))
                    csv_dict['TotalStartTimeUTC'] = totalStartTimeUTC
                elif line.startswith("TotalEndTimeUTC"):
                    totalEndTimeUTC = str(int(line[line.index("=")+1:]))
                    csv_dict['TotalEndTimeUTC'] = totalEndTimeUTC
                elif line.startswith("AccumulationID"):
                    accumulationID = str(int(line[line.index("=")+1:]))
                    csv_dict['AccumulationID'] = accumulationID 

        #Read the Model.csv file
        metric_id_name_dict = dict()
        with codecs.open(os.path.join(base_dir, str(model_id), f'{model_id}.csv'), "r", "utf-8") as f:
            version = f.readline().strip();
            f.readline();
            line3s = f.readline().split(',');

            modelName         = line3s[0].strip('"');
            csv_dict['ModelName'] = modelName

            modelType         = line3s[1].strip('"');
            csv_dict['ModelType'] = modelType

            modelCreateType   = line3s[2].strip('"');
            csv_dict['ModelCreateType'] = modelCreateType

            modelComment      = line3s[3].strip('"');
            csv_dict['ModelComment'] = modelComment

            createUserName    = line3s[4].strip('"');
            csv_dict['CreateUserName'] = createUserName

            modelRegisterDate = line3s[5].strip('"');
            csv_dict['ModelRegisterDate'] = modelRegisterDate

            modelCreateTiming = line3s[6].strip('"');
            csv_dict['ModelCreateTiming'] = modelCreateTiming

            modelCreateTimeUTC  = line3s[7].strip('"');
            csv_dict['ModelCreateTimeUTC'] = modelCreateTimeUTC

            modelCreateTime   = line3s[8].strip('"');
            csv_dict['ModelCreateTime'] = modelCreateTime

            analysisInterval  = line3s[9].strip('"');
            csv_dict['AnalysisInterval'] = analysisInterval

            invariantCount    = line3s[10].strip('"');
            # [11] def file name
            # [12] dat file name
            infoCount         = line3s[13].strip('"');
            csv_dict['MeasureInfoCount'] = infoCount

            metricCount       = line3s[14].strip('"');
            csv_dict['MetricCount'] = metricCount

            groupCount        = line3s[15].strip('"');
            csv_dict['MetricGroupCount'] = groupCount

            ex_metricCount    = line3s[16].strip('"');
            csv_dict['ExcludeMetricCount'] = ex_metricCount

            ex_groupCount     = line3s[17].strip().strip('"');
            csv_dict['ExcludeMetricGroupCount'] = ex_groupCount

            f.readline();

            totalStartTimeUTC = 9999999999999
            totalEndTimeUTC = 0
            csv_dict['MeasureInfo'] = []
            i = 0
            for line in f:
                line = line.strip()
                if not line: # 空行
                    break
                line_tm = line.split(',')
                start_time_utc = line_tm[0].strip('"')
                end_time_utc   = line_tm[3].strip('"')
                csv_dict['MeasureInfo'].append({'StartTimeUTC': start_time_utc, 'EndTimeUTC': end_time_utc})
                i += 0

            # metric list
            metricID = ""
            for line in f:
                line = line.strip()
                if not line: # 空行
                    break
                splitted = list(csv.reader([line]))[0]
                metric_id_name_dict[int(splitted[0])] = splitted[1]
                metricID += splitted[1] + ";"
            metricID = metricID.rstrip(';')
            csv_dict['MetricID'] = metricID

            # group list
            metricGroupID = ""
            for line in f:
                line = line.strip()
                if not line: # 空行
                    break
                splitted = list(csv.reader([line]))[0]
                metricGroupID += splitted[1] + ";"
            metricGroupID = metricGroupID.rstrip(';')
            csv_dict['MetricGroupID'] = metricGroupID

            # exclude metric list
            exMetricID = ""
            for line in f:
                line = line.strip()
                if not line: # 空行
                    break
                splitted = list(csv.reader([line]))[0]
                metric_id_name_dict[int(splitted[0])] = splitted[1]
                exMetricID += splitted[1] + ";"
            exMetricID = exMetricID.rstrip(';')
            csv_dict['ExcludeMetricID'] = exMetricID

            # exclide group list
            exMetricGroupID = ""
            for line in f:
                line = line.strip()
                if not line: # 空行
                    break
                splitted = list(csv.reader([line]))[0]
                exMetricGroupID += splitted[1] + ";"
            exMetricGroupID = exMetricGroupID.rstrip(';')
            csv_dict['ExcludeMetricGroupID'] = exMetricGroupID

            #The First line of the csv file will be a comment followed by ModelComment and ModelName
            #Rest all the key values are going to be dumped

            csv_str = '#'
            csv_str += f"ModelComment:{csv_dict['ModelComment']},"
            del(csv_dict['ModelComment'])
            csv_str += f"ModelName:{csv_dict['ModelName']},"
            del(csv_dict['ModelName'])
            for x in csv_dict.items():
                key = x[0]
                value = x[1]

                if key == 'MeasureInfo':
                    value = str(value)
                    value = ';'.join(value.split(','))
                if not value:
                    value = None
                csv_str += f'{key}:{value},'

        #Read the Binary file Model_data.dat file
        dat_info_dict = dict()
        with open(os.path.join(base_dir, str(model_id), f'{model_id}_data.dat'), "rb") as f:
            buf = f.read(356)     # 4 + 24 + 24 + 4 + 4 + 257 +(3)+ 4 + 8 + 8 + 4 + 4 + 4 + 4
            unpacked = struct.unpack("<I24s24sII257s3sIQQII", buf[0:348])
            fileVersion = unpacked[0]
            engineModelID = unpacked[1].decode().strip('\0')
            originalEngineModelID = unpacked[2].decode().strip('\0')
            modelVersion = unpacked[3]
            modelType = unpacked[4]
            #Adding this because model group name has an an unpredictable
            #encoding type of which keeps changing from time to time
            encoding_type = chardet.detect(unpacked[5])['encoding']

            if encoding_type == None:
                modelGroupName = ''
            else:
                modelGroupName = unpacked[5].decode(encoding_type).encode('utf-8').decode().strip('\0')
            analysisInterval = unpacked[7]
            totalCount = unpacked[8]
            invariantsCount = unpacked[9]
            dim = unpacked[10]
            delay = unpacked[11]

            dat_info_dict['FileVersion'] = fileVersion
            dat_info_dict['ModelVersion'] = modelVersion
            dat_info_dict['ModelType'] = modelType
            dat_info_dict['AnalysisInterval'] = analysisInterval
            dat_info_dict['TotalCount'] = totalCount
            dat_info_dict['InvariantsCount'] = invariantsCount
            dat_info_dict['Dim'] = dim
            dat_info_dict['Delay'] = delay

            for x in dat_info_dict.items():
                key = x[0]
                value = x[1]
                csv_str += f'{key}:{value},'
            csv_str = csv_str.rstrip(',')
            csv_str += '\n'

            inv_size = 8 + 8 + 4 + 4 + 4 + 4 + 4 + 4 + (4 * dim)
            list_of_dict = list()
            def gen():
                while True:
                    buf = f.read(inv_size)
                    if buf:
                        yield buf
                    else:
                        break
            for buf in gen():
                od_dict = collections.OrderedDict()
                unpacked = struct.unpack("<QQIIIIff", buf[0:40])
                filtered = unpacked[2]
                n = unpacked[3]
                m = unpacked[4]
                k = unpacked[5]
                fitness = unpacked[6]
                threshold = unpacked[7]
                theta_list = struct.unpack("<" + "f"*dim, buf[40:])
                theta = ",".join(str(x) for x in theta_list[0:n+m+2])
                od_dict['u'] = metric_id_name_dict[unpacked[0]]
                od_dict['y'] = metric_id_name_dict[unpacked[1]]
                od_dict['n'] = n
                od_dict['m'] = m
                od_dict['k'] = k
                od_dict['filtered'] = filtered
                od_dict['fitness'] = fitness
                od_dict['threshold'] = threshold
                od_dict['theta'] = theta
                list_of_dict.append(od_dict)

            #Converting the list of dicts to a temp dataframe
            data = pd.DataFrame(list_of_dict)
            #Find the max theta
            theta_list = data['theta'].to_list()
            #Assume first theta as max first
            max_t = len(theta_list[0].split(','))
            #Compare it with the rest and find out the max
            for x in theta_list[1:]:
                if len(x.split(',')) > max_t:
                    max_t = len(x.split(','))
            #Its a list of ordered dict, iterate through each dict
            inv_count = len(list_of_dict)
            for inv in list_of_dict:
                theta_list = inv['theta'].split(',')
                #Find out the current length of theta
                count = len(theta_list)
                while count <= max_t:
                    #Append None upto max theta if its less than max
                    theta_list.append(None)
                    count += 1
                #delete theta and create theta_0..upto theta_n..
                inv.pop('theta')
                i = 0
                while i < max_t:
                    inv[f'theta_{i}'] = theta_list[i]
                    i += 1

            inv_data = pd.DataFrame(list_of_dict)

        with open(csv_file, 'w') as filename:
            filename.write(csv_str)

        with open(csv_file, 'a') as filename:
            inv_data.to_csv(filename, index=False)

    def csv_to_mdl(self, filename, import_dir, server_timezone):
        r"""Converts the CSV file to MDL file which SIAT understands

        Parameters
        ----------
        filename    : str
                      The name of the csv file that needs to be converted

        import_dir  : str
                      Directory where the converted files needs to be stored

        Returns
        -------
        None

        """
        #Reading the CSV file
        info_dict = self._create_model_dat_info(filename)
        m_info_dict = info_dict['ModelInfo']
        d_info_dict = info_dict['Dat_Info']
        import_data = pd.read_csv(filename, comment='#')
        NewLine = "\n";

        model_id = 216431717102125120 + random.randint(0, 0xffffffff); # temporary
        self.log.info("Created Temp Model ID = %d" % model_id);

        tmp_metric_id = 72315979270460015;        # temporary
        engine_model_id = 155125710302553412109;  # temporary

        # ModelInfo
        modelName = f'Model.{int(time.time())}'
        self.model_name = modelName
        modelType = m_info_dict["ModelType"]
        modelCreateType = m_info_dict["ModelCreateType"]
        baseModelID = m_info_dict["BaseModelID"]
        modelLearnType = m_info_dict["ModelLearnType"]
        learningType = m_info_dict["LearningType"]
        modelCreateTiming = m_info_dict["ModelCreateTiming"]
        modelCreateTimeUTC = m_info_dict["ModelCreateTimeUTC"]
        modelCreateTime = m_info_dict["ModelCreateTime"]
        modelRegisterDate = m_info_dict["ModelRegisterDate"]
        #Model Comments as per req
        current_time = self.get_time_for_tzone(server_timezone)
        old_comment = m_info_dict["ModelComment"]
        new_comment = f'Model {modelName} imported at {current_time}'
        new_comment += f' based on {old_comment}'

        if len(new_comment) > 128:
            new_comment = new_comment[:125] + '...'

        modelComment = new_comment

        createUserName = m_info_dict["CreateUserName"]
        thresholdEither = m_info_dict["ThresholdEither"]
        thresholdBoth = m_info_dict["ThresholdBoth"]
        analysisInterval = m_info_dict["AnalysisInterval"]
        significantFigures = m_info_dict["SignificantFigures"]
        negativeInvariantsHolds = m_info_dict["NegativeInvariantsHolds"]
        calcParamN = m_info_dict["CalcParamN"]
        calcParamM = m_info_dict["CalcParamM"]
        calcParamK = m_info_dict["CalcParamK"]
        severity = m_info_dict["Severity"]
        metricCount = m_info_dict["MetricCount"]
        metricGroupCount = m_info_dict["MetricGroupCount"]
        excludeMetricCount = m_info_dict["ExcludeMetricCount"]
        excludeMetricGroupCount = m_info_dict["ExcludeMetricGroupCount"]
        excludeAutoRegressive = m_info_dict["ExcludeAutoRegressive"]
        fitnessThreshold = m_info_dict["FitnessThreshold"]
        autoRegressiveRange = m_info_dict["AutoRegressiveRange"]
        autoAnalysisUpdateFlag = m_info_dict["AutoAnalysisUpdateFlag"]
        totalStartTimeUTC = m_info_dict["TotalStartTimeUTC"]
        totalEndTimeUTC = m_info_dict["TotalEndTimeUTC"]
        accumulationID = m_info_dict["AccumulationID"]
        measureInfoCount = m_info_dict["MeasureInfoCount"]

        metricID = m_info_dict["MetricID"]
        metricGroupID = m_info_dict["MetricGroupID"]
        ex_metricID = m_info_dict["ExcludeMetricID"]
        ex_metricGroupID = m_info_dict["ExcludeMetricGroupID"]

        mes_info_list = m_info_dict["MeasureInfo"]
        mesInfo = []
        for info in mes_info_list:
            mm = '"' + str(info["StartTimeUTC"]) + '","","0","' + str(info["EndTimeUTC"]) + '","","0"'
            mesInfo.append(mm)

        metricList = metricID.split(';')
        metricGroupList = metricGroupID.split(';')
        exmetricList = ex_metricID.split(';')
        exmetricGroupList = ex_metricGroupID.split(';')

        group_name = m_info_dict['MetricGroupID']
        group_id = self._get_group_id_from_name(group_name)
        metric_name_id_dict = self._create_metric_dict(group_id)
        group_name_id_dict = self._create_metric_group_dict()

        modelVersion = d_info_dict["ModelVersion"]
        totalCount = d_info_dict["TotalCount"]
        invariantsCount = d_info_dict["InvariantsCount"]
        dim = d_info_dict["Dim"]
        delay = d_info_dict["Delay"]

        inv_total_count = import_data.shape[0]
        inv_count = import_data[import_data.filtered < 2].count()['filtered']
        now = datetime.datetime.now(datetime.timezone.utc)

        #---------------------------------------------------------------------------
        # <ModelID>.csv
        #---------------------------------------------------------------------------
        if not os.path.exists(os.path.join(import_dir, str(model_id))):
        # if not os.path.exists(posixpath.join(import_dir, str(model_id))):            
            # os.makedirs(os.path.join(import_dir, str(model_id)))
            os.makedirs(posixpath.join(import_dir, str(model_id)))
        with open(os.path.join(import_dir, str(model_id), f'{model_id}.csv'),
        # with open(posixpath.join(import_dir, str(model_id), f'{model_id}.csv'),        
                "w", newline="\n", encoding='utf-8') as f:
            f.write("3")                              # version = 3
            f.write('\n');
            f.write("\n");

            dataPart = ','.join( '"%s"' % x for x in [modelName,
                                                     modelType,
                                                     modelCreateType,
                                                     modelComment,
                                                     createUserName,
                                                     str(int(now.timestamp()*1000)),
                                                     modelCreateTiming,
                                                     modelCreateTimeUTC,
                                                     modelCreateTime,
                                                     analysisInterval,
                                                     str(inv_count),
                                                     "%d_def.dat" % model_id,
                                                     "%d_data.dat" % model_id,
                                                     str(len(mesInfo)),
                                                     metricCount,
                                                     metricGroupCount,
                                                     excludeMetricCount,
                                                     excludeMetricGroupCount
                                                    ]
                               );
            f.write(dataPart);
            f.write("\n");
            f.write("\n");
            # 計測データ期間
            for mes in mesInfo:
                f.write(mes)
                f.write("\n")
            f.write("\n");

            # 計測項目
            for met in metricList:
                f.write( '"' + str(metric_name_id_dict[met][0]) + '","' + met + '","' + str(metric_name_id_dict[met][1]) + '"' ) 
                f.write(NewLine);
            f.write(NewLine);
            
            # 計測項目グループ
            for grp in metricGroupList:
                if len(grp) == 0:
                    break
                f.write( '"' + str(group_name_id_dict[grp]) + '","' + grp + '"' ) 
                f.write(NewLine);
            f.write(NewLine);

            # 除外対象計測項目
            for met in exmetricList:
                if len(met) == 0:
                    break
                f.write( '"' + str(metric_name_id_dict[met][0]) + '","' + met + '","' + str(metric_name_id_dict[met][1]) + '"' ) 
                f.write(NewLine);
            f.write(NewLine);
            
            # 除外対象計測項目グループ
            for grp in exmetricGroupList:
                if len(grp) == 0:
                    break
                f.write( '"' + str(group_name_id_dict[grp]) + '","' + grp + '"' ) 
                f.write(NewLine);
            f.write(NewLine);


        #---------------------------------------------------------------------------
        # <ModelID>_def.dat
        #---------------------------------------------------------------------------
        with open(os.path.join(import_dir, str(model_id), f'{model_id}_def.dat'), "wb") as f:
        # with open(posixpath.join(import_dir, str(model_id), f'{model_id}_def.dat'), "wb") as f:            
            encoding = "utf-16-le";
            writer = lambda s: f.write(s.encode(encoding));
            NewLine = "\r\n";
            writer('0,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
            writer(NewLine);
            writer(NewLine.join(x for x in ["{",
                                                              "NodeType=22",
                                                              "CreateTiming=%s" % modelCreateTiming,
                                                              "SignificantFigures=%s" % significantFigures,
                                                              "BkModelDataMetricIDCount=0",
                                                              "ModelType=%s" % modelType,
                                                              "ModelCreateType=%s" % modelCreateType,
                                                              "OriginalEngineModelID=",
                                                              "TotalStartTimeUTC=%s" % totalStartTimeUTC,
                                                              "DeleteTimeUTC=0",
                                                              "BkFilteringStatus=0",
                                                              "BkEngineModelID=",
                                                              "ModelName=%s" % modelName,
                                                              "FilteringEngineModelID=",
                                                              "CreateUserName=Converter",
                                                              "ThresholdEither=%d" % int(float(thresholdEither) * 100),
                                                              "JobID=",
                                                              "NegativeInvariantsHolds=%s" % negativeInvariantsHolds,
                                                              "AutoRegressiveRange=%s" % autoRegressiveRange,
                                                              "BaseModelID=%s" % baseModelID,
                                                              "CreateTime=%s" % modelCreateTime,
                                                              "ModelDataMetricIDCount=%d"% int(metricCount),
                                                              "Comment=%s" % modelComment,
                                                              "FitnessThreshold=%s" % fitnessThreshold,
                                                              "TempEngineModelID=",
                                                              "MetricDataRequestID=0",
                                                              "Severity=%s" % severity,
                                                              "ModelID=0",                     # <<<<<<<<<<<<<<<<<
                                                              "CreateTimeUTC=%s" % modelCreateTimeUTC,
                                                              "CalcParamK=%s" % calcParamK,
                                                              "FilteringInvariantsCount=0",
                                                              "AnalysisInterval=%s" % analysisInterval,
                                                              "MetricDataID=0",
                                                              "ModelLearnType=%s" % modelLearnType,
                                                              "LearningType=%s" % learningType,
                                                              "CalcParamM=%s" % calcParamM,
                                                              "ExcludeAutoRegressive=%s" % excludeAutoRegressive,
                                                              "CalcParamN=%s" % calcParamN,
                                                              "AutoAnalysisUpdateFlag=%s" % autoAnalysisUpdateFlag,
                                                              "AccumulationID=%s" % accumulationID,
                                                              "FileVersion=2",
                                                              "EngineModelID=",
                                                              "TotalEndTimeUTC=%s" % totalEndTimeUTC,
                                                              "FilteringStatus=0",
                                                              "OriginalModelID=0",
                                                              "BaseEngineModelID=",
                                                              "ThresholdBoth=%d" % int(float(thresholdBoth) * 100),
                                                              "}"]));
            writer(NewLine);

            # 計測データ期間親ノード
            writer("{{");
            writer(NewLine);
            writer('56de1af7,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
            writer(NewLine);
            writer("{");
            writer(NewLine);
            writer("NodeType=28");
            writer(NewLine);
            writer("}");
            writer(NewLine);

            # 計測データ期間子ノード（繰り返しあり）
            writer("{{");
            for info in mes_info_list:
                writer(NewLine);
                writer('56d00b6e,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
                writer(NewLine);
                writer(NewLine.join(x for x in ["{",
                                                "NodeType=29",
                                                "EndTime=",
                                                "EndPrevDayFlag=0",
                                                "StartTimeUTC=%s" % str(info["StartTimeUTC"]),
                                                "EndTimeUTC=%s" % str(info["EndTimeUTC"]),
                                                "StartTime=",
                                                "StartPrevDayFlag=0",
                                                "}"
                                                ]));
            writer(NewLine);
            writer("}}");
            writer(NewLine);
            
            # モデル化対象計測データ期間履歴親ノード
            writer('56d00b6d,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
            writer(NewLine);
            writer(NewLine.join(x for x in ["{",
                                            "NodeType=30",
                                            "}"]));
            writer(NewLine);

            # モデル作成開始時刻ノード？
            writer("{{");
            writer(NewLine);
            writer('56d00b6c,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
            writer(NewLine);
            writer(NewLine.join(x for x in ["{",
                                            "NodeType=31",
                                            "PastOperation=0",
                                            "ExecTimeUTC=%s" % totalStartTimeUTC,
                                            "BaseModelID=0",
                                            "BaseModelName=",
                                            "}"
                                            ]));
            writer(NewLine);

            # モデル化対象計測データ期間履歴子ノード（繰り返しあり）
            writer("{{");
            for info in mes_info_list:
                writer(NewLine);
                writer('56d00b6b,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
                writer(NewLine);
                writer(NewLine.join(x for x in ["{",
                                               "NodeType=32",
                                               "StartTimeUTC=%s" % str(info["StartTimeUTC"]),
                                               "EndTimeUTC=%s" % str(info["EndTimeUTC"]),
                                               "}"
                                               ]));
            writer(NewLine);
            writer("}}");
            writer(NewLine);
            writer("}}");
            writer(NewLine);
            writer("}}");
            writer(NewLine);

        # <ModelID>_data.dat
        with open(os.path.join(import_dir, str(model_id), f'{model_id}_data.dat'), "wb") as f:
        # with open(posixpath.join(import_dir, str(model_id), f'{model_id}_data.dat'), "wb") as f:            
            f.write(struct.pack("<i", 1));                 # version
            f.write(b"%023d" % engine_model_id);           # engin model id
            f.write(b"\x00");                              #
            f.write(b"%023d" % engine_model_id);           # original engine model id
            f.write(b"\x00");                              #
            f.write(struct.pack("<i", 1));                 # model version
            f.write(struct.pack("<i", 0));                 # model type
            f.write(b"0" * 256);                           # model group name
            f.write(b"\x00");                              # 
            f.write(b"\x00" * 3);                          # padding
            f.write(struct.pack("<I", int(analysisInterval)));  #
            f.write(struct.pack("<Q", inv_total_count));   # 総相関数
            f.write(struct.pack("<Q", inv_count));         # 有効相関数
            f.write(struct.pack("<I", int(dim)));          # dim値
            f.write(struct.pack("<I", int(delay)));        # delay
            f.write(struct.pack("<I", 0));                 # reserved1
            f.write(struct.pack("<I", 0));                 # reserved2
            
            for index, row in import_data.iterrows():
                uname = row['u']
                yname = row['y']
                filtered = int(row['filtered'])
                n = int(row['n'])
                m = int(row['m'])
                k = int(row['k'])
                fitness = float(row['fitness'])
                threshold = float(row['threshold'])
                #Row 8 to N are thetas, combining them as a list
                theta_list = [float(x) for x in row[8:].values if str(x) != 'nan']
                
                f.write(struct.pack("<Q", metric_name_id_dict[uname][0]));
                f.write(struct.pack("<Q", metric_name_id_dict[yname][0]));
                f.write(struct.pack("<I", filtered)); # filtered
                f.write(struct.pack("<I", n)); # n
                f.write(struct.pack("<I", m)); # m
                f.write(struct.pack("<I", k)); # k
                f.write(struct.pack("<f", fitness));
                f.write(struct.pack("<f", threshold));
                # theta
                for theta in theta_list:
                    f.write(struct.pack("<f", theta));
                for i in range(int(dim) - len(theta_list)):
                    f.write(struct.pack("<I", 0)); # padding
        '''
        # Make mdl file
        with zipfile.ZipFile("%d.mdl" % model_id, "w", zipfile.ZIP_DEFLATED) as f:
            os.mkdir("%d" % model_id);
            f.write("%d" % model_id);
            f.write("%d.csv" % model_id, "%d/%d.csv" % (model_id, model_id));
            f.write("%d_data.dat" % model_id, "%d/%d_data.dat" % (model_id, model_id));
            f.write("%d_def.dat" % model_id, "%d/%d_def.dat" % (model_id, model_id));

        # Cleanup
        cleanup = False
        if cleanup:
            os.rmdir("%d" % model_id);
            os.remove("%d.csv" % model_id);
            os.remove("%d_data.dat" % model_id);
            os.remove("%d_def.dat" % model_id);
        '''

        ### NEEDS SOME PIECE CODE OF ASSIGN THE UNIX PATH TO A VARIABLE AND RETURN IT
        return os.path.join(import_dir, str(model_id))
        # return posixpath.join(import_dir, str(model_id))        

