# -*- coding: utf-8 -*-
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
from xml.dom import minidom
import csv
import sys;
import struct;
import random;
import os;
import codecs;
import subprocess;
import getopt
import datetime
import zipfile
from siatmgrconnector import SiatMgrConnector;


################################################################################
class ModelXML():
    def __init__(self):
        self.Root = Element("Invariants");
        self.Root.set("model", "com.necla.asds.engine.core.ARXWithCModel");

################################################################################
def GetMetricsDictFromServer(server):
    name_id_dict = {};
    conn = SiatMgrConnector(server);
    metric_list = conn.GetMetricsList();
    for met in metric_list:
        id_list = [0]*2;
        id = met["MetricID"]
        grpCnt = 0
        if int(met["GroupCount"]) > 0:
             grpCnt = 1
        id_list[0] = id
        id_list[1] = grpCnt
        name_id_dict[met["MetricName"]] = id_list
    return name_id_dict

def GetMetricGroupDictFromServer(server):
    name_id_dict = {};
    conn = SiatMgrConnector(server);
    group_list = conn.GetGroupList();
    for grp in group_list:
        id = grp["GroupID"]
        name_id_dict[grp["GroupName"]] = id
    return name_id_dict

################################################################################
def Xml2Mdl(filename, model_name="", server=""):

    NewLine = "\n";

    xml_root = ElementTree.parse(filename).getroot();

    model_id = 216431717102125120 + random.randint(0, 0xffffffff); # temporary
    print("Created Model ID = %d" % model_id);

    tmp_metric_id = 72315979270460015;        # temporary
    engine_model_id = 155125710302553412109;  # temporary

    # ModelInfo
    info_node = xml_root.find("ModelInfo")
    modelName = info_node.find("ModelName").text or ""
    modelType = info_node.find("ModelType").text or ""
    modelCreateType = info_node.find("ModelCreateType").text or ""
    baseModelID = info_node.find("BaseModelID").text or ""
    modelLearnType = info_node.find("ModelLearnType").text or ""
    learningType = info_node.find("LearningType").text or ""
    modelCreateTiming = info_node.find("ModelCreateTiming").text or ""
    modelCreateTimeUTC = info_node.find("ModelCreateTimeUTC").text or ""
    modelCreateTime = info_node.find("ModelCreateTime").text or ""
    modelRegisterDate = info_node.find("ModelRegisterDate").text or ""
    modelComment = info_node.find("ModelComment").text or ""
    createUserName = info_node.find("CreateUserName").text or ""
    thresholdEither = info_node.find("ThresholdEither").text or ""
    thresholdBoth = info_node.find("ThresholdBoth").text or ""
    analysisInterval = info_node.find("AnalysisInterval").text or ""
    significantFigures = info_node.find("SignificantFigures").text or ""
    negativeInvariantsHolds = info_node.find("NegativeInvariantsHolds").text or ""
    calcParamN = info_node.find("CalcParamN").text or ""
    calcParamM = info_node.find("CalcParamM").text or ""
    calcParamK = info_node.find("CalcParamK").text or ""
    severity = info_node.find("Severity").text or ""
    metricCount = info_node.find("MetricCount").text or ""
    metricGroupCount = info_node.find("MetricGroupCount").text or ""
    excludeMetricCount = info_node.find("ExcludeMetricCount").text or ""
    excludeMetricGroupCount = info_node.find("ExcludeMetricGroupCount").text or ""
    excludeAutoRegressive = info_node.find("ExcludeAutoRegressive").text or ""
    fitnessThreshold = info_node.find("FitnessThreshold").text or ""
    autoRegressiveRange = info_node.find("AutoRegressiveRange").text or ""
    autoAnalysisUpdateFlag = info_node.find("AutoAnalysisUpdateFlag").text or ""
    totalStartTimeUTC = info_node.find("TotalStartTimeUTC").text or ""
    totalEndTimeUTC = info_node.find("TotalEndTimeUTC").text or ""
    accumulationID = info_node.find("AccumulationID").text or ""
    measureInfoCount = info_node.find("MeasureInfoCount").text or ""

    mesureinfo_nodes = info_node.findall("MeasureInfo")
    metricID = info_node.find("MetricID").text or ""
    metricGroupID = info_node.find("MetricGroupID").text or ""
    ex_metricID = info_node.find("ExcludeMetricID").text or ""
    ex_metricGroupID = info_node.find("ExcludeMetricGroupID").text or ""

    if len(model_name) > 1:
        modelName = model_name

    # 
    mesInfo = []
    for mes_info in mesureinfo_nodes:
        mm = '"' + mes_info.find("StartTimeUTC").text + '","","0","' + mes_info.find("EndTimeUTC").text + '","","0"'
        mesInfo.append(mm)

    metricList = metricID.split(',')
    metricGroupList = metricGroupID.split(',')
    exmetricList = ex_metricID.split(',')
    exmetricGroupList = ex_metricGroupID.split(',')

    #
    metric_name_id_dict = dict();
    group_name_id_dict = dict();
    if server != "":
         metric_name_id_dict = GetMetricsDictFromServer(server)
         group_name_id_dict = GetMetricGroupDictFromServer(server)

    info_node = xml_root.find("Dat_Info")
    modelVersion = info_node.find("ModelVersion").text or ""
    totalCount = info_node.find("TotalCount").text or ""
    invariantsCount = info_node.find("InvariantsCount").text or ""
    dim = info_node.find("Dim").text or ""
    delay = info_node.find("Delay").text or ""

    inv_nodes = xml_root.findall("Invariant");

    inv_total_count = 0
    inv_count = 0

    for inv in inv_nodes:
        uname = inv.find("u").text;
        yname = inv.find("y").text;
        # tmp_metric_id = add_metric(uname, tmp_metric_id);
        # tmp_metric_id = add_metric(yname, tmp_metric_id);
        inv_total_count += 1;
        if int(inv.find("isfiltered").text) < 2:
            inv_count += 1;

    now = datetime.datetime.now(datetime.timezone.utc)
    # print(str(int(now.timestamp()*1000)))


    #---------------------------------------------------------------------------
    # <ModelID>.csv
    #---------------------------------------------------------------------------
    with open("%d.csv" % model_id, "w", newline="\n", encoding='utf-8') as f:
        f.write("3")                              # version = 3
        f.write('\n');
        f.write("\n");

        dataPart = ','.join( '"%s"' % x for x in [modelName,                       # model name
                                                 modelType,                       # model type
                                                 modelCreateType,                 # model create type
                                                 modelComment,                    # comment
                                                 createUserName,                  # create user name
                                                 str(int(now.timestamp()*1000)),  # 作成日時(ミリ秒)
                                                 modelCreateTiming,               # 作成タイミング
                                                 modelCreateTimeUTC,              # 作成開始時刻
                                                 modelCreateTime,                 # 
                                                 analysisInterval,                # 分析間隔
                                                 str(inv_count),                  # 有効相関数
                                                 "%d_def.dat" % model_id,         # モデルインポート定義ファイル名
                                                 "%d_data.dat" % model_id,        # モデルインポートデータファイル名
                                                 str(len(mesInfo)),               # 計測データ期間数
                                                 metricCount,                     # 計測項目数
                                                 metricGroupCount,                # 計測項目グループ数
                                                 excludeMetricCount,              # 除外対象の計測項目数
                                                 excludeMetricGroupCount          # 除外対象の計測項目グループ数
                                                ]
                           );
        f.write(dataPart);
        f.write("\n");
        f.write("\n");

        # 計測データ期間
        for mes in mesInfo:
            f.write(mes)
            f.write("\n")
        f.write("\n");

        # 計測項目
        for met in metricList:
            f.write( '"' + str(metric_name_id_dict[met][0]) + '","' + met + '","' + str(metric_name_id_dict[met][1]) + '"' ) 
            f.write(NewLine);
        f.write(NewLine);
        
        # 計測項目グループ
        for grp in metricGroupList:
            if len(grp) == 0:
                break
            f.write( '"' + str(group_name_id_dict[grp]) + '","' + grp + '"' ) 
            f.write(NewLine);
        f.write(NewLine);

        # 除外対象計測項目
        for met in exmetricList:
            if len(met) == 0:
                break
            f.write( '"' + str(metric_name_id_dict[met][0]) + '","' + met + '","' + str(metric_name_id_dict[met][1]) + '"' ) 
            f.write(NewLine);
        f.write(NewLine);
        
        # 除外対象計測項目グループ
        for grp in exmetricGroupList:
            if len(grp) == 0:
                break
            f.write( '"' + str(group_name_id_dict[grp]) + '","' + grp + '"' ) 
            f.write(NewLine);
        f.write(NewLine);


    #---------------------------------------------------------------------------
    # <ModelID>_def.dat
    #---------------------------------------------------------------------------
    with open("%d_def.dat" % model_id, "wb") as f:
        encoding = "utf-16-le";
        writer = lambda s: f.write(s.encode(encoding));
        NewLine = "\r\n";
        writer('0,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
        writer(NewLine);
        writer(NewLine.join(x for x in ["{",
                                                          "NodeType=22",
                                                          "CreateTiming=%s" % modelCreateTiming,
                                                          "SignificantFigures=%s" % significantFigures,
                                                          "BkModelDataMetricIDCount=0",                           # @@@@@@@
                                                          "ModelType=%s" % modelType,
                                                          "ModelCreateType=%s" % modelCreateType,
                                                          "OriginalEngineModelID=",                               # @@@@@@@
                                                          "TotalStartTimeUTC=%s" % totalStartTimeUTC, 
                                                          "DeleteTimeUTC=0",                                      # @@@@@@@
                                                          "BkFilteringStatus=0",                                  # @@@@@@@
                                                          "BkEngineModelID=",                                     # @@@@@@@
                                                          "ModelName=%s" % modelName,
                                                          "FilteringEngineModelID=",                              # @@@@@@@
                                                          "CreateUserName=Converter",
                                                          "ThresholdEither=%d" % int(float(thresholdEither) * 100),
                                                          "JobID=",                                               # @@@@@@@
                                                          "NegativeInvariantsHolds=%s" % negativeInvariantsHolds,
                                                          "AutoRegressiveRange=%s" % autoRegressiveRange,
                                                          "BaseModelID=%s" % baseModelID,
                                                          "CreateTime=%s" % modelCreateTime,
                                                          "ModelDataMetricIDCount=%d"% int(metricCount),
                                                          "Comment=%s" % modelComment,
                                                          "FitnessThreshold=%s" % fitnessThreshold,
                                                          "TempEngineModelID=",                                   # @@@@@@@
                                                          "MetricDataRequestID=0",                                # @@@@@@@
                                                          "Severity=%s" % severity,
                                                          "ModelID=0",                     # <<<<<<<<<<<<<<<<<
                                                          "CreateTimeUTC=%s" % modelCreateTimeUTC,
                                                          "CalcParamK=%s" % calcParamK,
                                                          "FilteringInvariantsCount=0",                           # @@@@@@@
                                                          "AnalysisInterval=%s" % analysisInterval,
                                                          "MetricDataID=0",                                       # @@@@@@@
                                                          "ModelLearnType=%s" % modelLearnType,
                                                          "LearningType=%s" % learningType,
                                                          "CalcParamM=%s" % calcParamM,
                                                          "ExcludeAutoRegressive=%s" % excludeAutoRegressive,
                                                          "CalcParamN=%s" % calcParamN,
                                                          "AutoAnalysisUpdateFlag=%s" % autoAnalysisUpdateFlag,
                                                          "AccumulationID=%s" % accumulationID,
                                                          "FileVersion=2",
                                                          "EngineModelID=",                                       # @@@@@@@
                                                          "TotalEndTimeUTC=%s" % totalEndTimeUTC,
                                                          "FilteringStatus=0",                                    # @@@@@@@
                                                          "OriginalModelID=0",                                    # @@@@@@@
                                                          "BaseEngineModelID=",                                   # @@@@@@@
                                                          "ThresholdBoth=%d" % int(float(thresholdBoth) * 100),
                                                          "}"]));
        writer(NewLine);

        # 計測データ期間親ノード
        writer("{{");
        writer(NewLine);
        writer('56de1af7,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
        writer(NewLine);
        writer("{");
        writer(NewLine);
        writer("NodeType=28");
        writer(NewLine);
        writer("}");
        writer(NewLine);

        # 計測データ期間子ノード（繰り返しあり）
        writer("{{");
        for mes_info in mesureinfo_nodes:
            writer(NewLine);
            writer('56d00b6e,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
            writer(NewLine);
            writer(NewLine.join(x for x in ["{",
                                            "NodeType=29",
                                            "EndTime=",
                                            "EndPrevDayFlag=0",
                                            "StartTimeUTC=%s" % mes_info.find("StartTimeUTC").text,
                                            "EndTimeUTC=%s" % mes_info.find("EndTimeUTC").text,
                                            "StartTime=",
                                            "StartPrevDayFlag=0",
                                            "}"
                                            ]));
        writer(NewLine);
        writer("}}");
        writer(NewLine);
        
        # モデル化対象計測データ期間履歴親ノード
        writer('56d00b6d,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
        writer(NewLine);
        writer(NewLine.join(x for x in ["{",
                                        "NodeType=30",
                                        "}"]));
        writer(NewLine);

        # モデル作成開始時刻ノード？
        writer("{{");
        writer(NewLine);
        writer('56d00b6c,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
        writer(NewLine);
        writer(NewLine.join(x for x in ["{",
                                        "NodeType=31",
                                        "PastOperation=0",
                                        "ExecTimeUTC=%s" % totalStartTimeUTC,
                                        "BaseModelID=0",
                                        "BaseModelName=",
                                        "}"
                                        ]));
        writer(NewLine);

        # モデル化対象計測データ期間履歴子ノード（繰り返しあり）
        writer("{{");
        for mes_info in mesureinfo_nodes:
            writer(NewLine);
            writer('56d00b6b,"",0,0,0,36,36,10,10,c0c0c0,0,0,0,0,1,0,0,100,ffffff,0,"MS Shell Dlg","","%SYSTEM_FW_ROOT%\SVC\Image\Bitmap\Default.bmp","0' + "\u0012" + '0"');
            writer(NewLine);
            writer(NewLine.join(x for x in ["{",
                                           "NodeType=32",
                                           "StartTimeUTC=%s" % mes_info.find("StartTimeUTC").text,
                                           "EndTimeUTC=%s" % mes_info.find("EndTimeUTC").text,
                                           "}"
                                           ]));
        writer(NewLine);
        writer("}}");
        writer(NewLine);
        writer("}}");
        writer(NewLine);
        writer("}}");
        writer(NewLine);


    # <ModelID>_data.dat
    with open("%d_data.dat" % model_id, "wb") as f:
        f.write(struct.pack("<i", 1));                 # version
        f.write(b"%023d" % engine_model_id);           # engin model id
        f.write(b"\x00");                              #
        f.write(b"%023d" % engine_model_id);           # original engine model id
        f.write(b"\x00");                              #
        f.write(struct.pack("<i", 1));                 # model version
        f.write(struct.pack("<i", 0));                 # model type
        f.write(b"0" * 256);                           # model group name
        f.write(b"\x00");                              # 
        f.write(b"\x00" * 3);                          # padding
        f.write(struct.pack("<I", int(analysisInterval)));  #
        f.write(struct.pack("<Q", inv_total_count));   # 総相関数
        f.write(struct.pack("<Q", inv_count));         # 有効相関数
        f.write(struct.pack("<I", int(dim)));          # dim値
        f.write(struct.pack("<I", int(delay)));        # delay
        f.write(struct.pack("<I", 0));                 # reserved1
        f.write(struct.pack("<I", 0));                 # reserved2
        
        inv_nodes = xml_root.findall("Invariant");
        for inv in inv_nodes:
            uname = inv.find("u").text;
            yname = inv.find("y").text;
            isfiltered = int(inv.findtext("isfiltered"));
            n = int(inv.findtext("n"));
            m = int(inv.findtext("m"));
            k = int(inv.findtext("k"));
            fitness = float(inv.findtext("fitness"));
            threshold = float(inv.findtext("threshold"));
            theta_list = [float(x) for x in inv.findtext("theta").split(",")];
            
            f.write(struct.pack("<Q", metric_name_id_dict[uname][0]));
            f.write(struct.pack("<Q", metric_name_id_dict[yname][0]));
            f.write(struct.pack("<I", isfiltered)); # filtered
            f.write(struct.pack("<I", n)); # n
            f.write(struct.pack("<I", m)); # m
            f.write(struct.pack("<I", k)); # k
            f.write(struct.pack("<f", fitness));
            f.write(struct.pack("<f", threshold));
            # theta
            for theta in theta_list:
                f.write(struct.pack("<f", theta));
            for i in range(int(dim) - len(theta_list)):
                f.write(struct.pack("<I", 0)); # padding

    # # Make mdl file
    # with zipfile.ZipFile("%d.mdl" % model_id, "w", zipfile.ZIP_DEFLATED) as f:
    #     os.mkdir("%d" % model_id);
    #     f.write("%d" % model_id);
    #     f.write("%d.csv" % model_id, "%d/%d.csv" % (model_id, model_id));
    #     f.write("%d_data.dat" % model_id, "%d/%d_data.dat" % (model_id, model_id));
    #     f.write("%d_def.dat" % model_id, "%d/%d_def.dat" % (model_id, model_id));

    # # Cleanup
    # cleanup = True;
    # if cleanup:
    #     os.rmdir("%d" % model_id);
    #     os.remove("%d.csv" % model_id);
    #     os.remove("%d_data.dat" % model_id);
    #     os.remove("%d_def.dat" % model_id);

    return (model_id)

################################################################################
def PrintUsage():
    print("python modelXml2Mdl.py -s severIpAddress xmlFilename newModelName")
    print("python modelXml2Mdl.py -s severIpAddress xmlFilename")


################################################################################
if __name__ == "__main__":

    try:
        optlist, args = getopt.getopt(sys.argv[1:], 's:')
    except getopt.GetoptError:
        PrintUsage();
        sys.exit(1);

    serverAddr = ""

    for o, a in optlist:
        if o == "-s":
            serverAddr = a

    if len(serverAddr) == 0:
        PrintUsage();
        sys.exit(2);

    filename = args[0];

    if len(args) == 2:
        Xml2Mdl(filename, args[1], server=serverAddr);
    else:
        Xml2Mdl(filename, server=serverAddr);
