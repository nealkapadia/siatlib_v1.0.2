# -*- coding: utf-8 -*-
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement
from xml.dom import minidom
import csv
import sys
import struct
import random
import os
import codecs
import subprocess
import getopt
import zipfile
import shutil

################################################################################
class ModelXML():

    def __init__(self):
        self.Root = Element("Invariants")
        self.Root.set("model", "com.necla.asds.engine.core.ARXWithCModel")

    #---------------------------------------------------------------------------
    def AddModelInfoNode(self,
                         modelName, modelType,modelCreateType,
                         baseModelID, modelLearnType, learningType,
                         modelCreateTiming, modelCreateTimeUTC, modelCreateTime,
                         modelRegisterDate,
                         modelComment, createUserName,
                         thresholdEither, thresholdBoth,
                         analysisInterval, significantFigures, negativeInvariantsHolds,
                         calcParamN, calcParamM, calcParamK,
                         severity,
                         metricCount, groupCount, ex_metricCount, ex_groupCount,
                         excludeAutoRegressive, fitnessThreshold,
                         autoRegressiveRange, autoAnalysisUpdateFlag,
                         totalStartTimeUTC, totalEndTimeUTC, accumulationID,
                         invariantCount, infoCount
                         ):
        modelInfo = SubElement(self.Root, "ModelInfo")

        # ModelXML._add_text_node(modelInfo, "Version", str(version))
        # ModelXML._add_text_node(DefInfo, "FileVersion", fileVersion)
        ModelXML._add_text_node(modelInfo, "ModelName", str(modelName))
        ModelXML._add_text_node(modelInfo, "ModelType", str(modelType))
        ModelXML._add_text_node(modelInfo, "ModelCreateType", str(modelCreateType))
        ModelXML._add_text_node(modelInfo, "BaseModelID", baseModelID)                          # from _def file
        ModelXML._add_text_node(modelInfo, "ModelLearnType", modelLearnType)                    # from _def file
        ModelXML._add_text_node(modelInfo, "LearningType", learningType)                        # from _def file
        ModelXML._add_text_node(modelInfo, "ModelCreateTiming", str(modelCreateTiming))
        ModelXML._add_text_node(modelInfo, "ModelCreateTimeUTC", str(modelCreateTimeUTC))
        ModelXML._add_text_node(modelInfo, "ModelCreateTime", str(modelCreateTime))
        ModelXML._add_text_node(modelInfo, "ModelRegisterDate", str(modelRegisterDate))
        ModelXML._add_text_node(modelInfo, "ModelComment", str(modelComment))
        ModelXML._add_text_node(modelInfo, "CreateUserName", str(createUserName))
        ModelXML._add_text_node(modelInfo, "ThresholdEither", thresholdEither)                  # from _def file
        ModelXML._add_text_node(modelInfo, "ThresholdBoth", thresholdBoth)                      # from _def file
        ModelXML._add_text_node(modelInfo, "AnalysisInterval", str(analysisInterval))
        ModelXML._add_text_node(modelInfo, "SignificantFigures", significantFigures)            # from _def file
        ModelXML._add_text_node(modelInfo, "NegativeInvariantsHolds", negativeInvariantsHolds)  # from _def file
        ModelXML._add_text_node(modelInfo, "CalcParamN", calcParamN)                            # from _def file
        ModelXML._add_text_node(modelInfo, "CalcParamM", calcParamM)                            # from _def file
        ModelXML._add_text_node(modelInfo, "CalcParamK", calcParamK)                            # from _def file
        ModelXML._add_text_node(modelInfo, "Severity", severity)                                # from _def file
        ModelXML._add_text_node(modelInfo, "MetricCount", str(metricCount))
        ModelXML._add_text_node(modelInfo, "MetricGroupCount", str(groupCount))
        ModelXML._add_text_node(modelInfo, "ExcludeMetricCount", str(ex_metricCount))
        ModelXML._add_text_node(modelInfo, "ExcludeMetricGroupCount", str(ex_groupCount))
        ModelXML._add_text_node(modelInfo, "ExcludeAutoRegressive", excludeAutoRegressive)      # from _def file
        ModelXML._add_text_node(modelInfo, "FitnessThreshold", fitnessThreshold)                # from _def file
        ModelXML._add_text_node(modelInfo, "AutoRegressiveRange", autoRegressiveRange)          # from _def file
        ModelXML._add_text_node(modelInfo, "AutoAnalysisUpdateFlag", autoAnalysisUpdateFlag)    # from _def file
        ModelXML._add_text_node(modelInfo, "TotalStartTimeUTC", totalStartTimeUTC)              # from _def file
        ModelXML._add_text_node(modelInfo, "TotalEndTimeUTC", totalEndTimeUTC)                  # from _def file
        ModelXML._add_text_node(modelInfo, "AccumulationID", accumulationID)                    # from _def file
        # ModelXML._add_text_node(modelInfo, "InvariantsCount", str(invCount))
        ModelXML._add_text_node(modelInfo, "MeasureInfoCount", str(infoCount))

    def AddModelMeasureInfoNode(self, totalStartTimeUTC, totalEndTimeUTC):
        modelInfo = self.Root.find("ModelInfo")
        measureInfo = SubElement(modelInfo, "MeasureInfo")

        ModelXML._add_text_node(measureInfo, "StartTimeUTC", totalStartTimeUTC)
        ModelXML._add_text_node(measureInfo, "EndTimeUTC", totalEndTimeUTC)

    def AddModelMetricInfoNode(self, metricID, metricGroupID, exMetricID, exMetricGroupID):
        modelInfo = self.Root.find("ModelInfo")

        ModelXML._add_text_node(modelInfo, "MetricID", metricID)
        ModelXML._add_text_node(modelInfo, "MetricGroupID", metricGroupID)
        ModelXML._add_text_node(modelInfo, "ExcludeMetricID", exMetricID)
        ModelXML._add_text_node(modelInfo, "ExcludeMetricGroupID", exMetricGroupID)

    #---------------------------------------------------------------------------
    def AddInfoNode(self, fileVersion,
                    engineModelID, originalEngineModelID,
                    modelVersion, modelType, modelGroupName,
                    analysisInterval,
                    totalCount, invariantsCount,
                    dim, delay):
        info = SubElement(self.Root, "Dat_Info")

        ModelXML._add_text_node(info, "FileVersion", str(fileVersion))
        # ModelXML._add_text_node(info, "EngineModelID", engineModelID)
        # ModelXML._add_text_node(info, "OriginalEngineModelID", originalEngineModelID)
        ModelXML._add_text_node(info, "ModelVersion", str(modelVersion))
        ModelXML._add_text_node(info, "ModelType", str(modelType))
        # ModelXML._add_text_node(info, "ModelGroupName", modelGroupName)
        ModelXML._add_text_node(info, "AnalysisInterval", str(analysisInterval))
        ModelXML._add_text_node(info, "TotalCount", str(totalCount))
        ModelXML._add_text_node(info, "InvariantsCount", str(invariantsCount))
        ModelXML._add_text_node(info, "Dim", str(dim))
        ModelXML._add_text_node(info, "Delay", str(delay))

    #---------------------------------------------------------------------------
    def AddInvariantNode(self, uname, yname, isfiltered, n, m, k,
                         fitness, threshold,
                         theta):
        inv = SubElement(self.Root, "Invariant")

        ModelXML._add_text_node(inv, "u", uname)
        ModelXML._add_text_node(inv, "y", yname)
        ModelXML._add_text_node(inv, "isfiltered", str(isfiltered))
        ModelXML._add_text_node(inv, "n", str(n))
        ModelXML._add_text_node(inv, "m", str(m))
        ModelXML._add_text_node(inv, "k", str(k))
        ModelXML._add_text_node(inv, "fitness", str(fitness))
        ModelXML._add_text_node(inv, "threshold", str(threshold))
        ModelXML._add_text_node(inv, "theta", theta)

    @classmethod
    #---------------------------------------------------------------------------
    def _add_text_node(cls, parent, node_name, text):
        node = SubElement(parent, node_name)
        node.text = text

    #---------------------------------------------------------------------------
    def ToString(self):
        raw_str = ElementTree.tostring(self.Root, "utf-8")
        reparsed = minidom.parseString(raw_str)
        return reparsed.toprettyxml(indent="	")[23:] # DELETE XML declaration header


################################################################################
def Mdl2Xml(file_id, dirname):


    # base_dir = os.path.dirname(filename)
    # file_id = os.path.splitext(os.path.basename(filename))[0]

    base_dir = dirname
    xml_root = ModelXML()

    #---------------------------------------------------------------------------
    # <id>_def.dat
    #---------------------------------------------------------------------------
    # with codecs.open(os.path.join(base_dir, "%s_def.dat" % file_id), "r", "utf-16-le") as f:
    with codecs.open("%s/%s_def.dat" % (base_dir, file_id), "r", "utf-16-le") as f:
        for line in f:
            if line.startswith("FileVersion"):
                fileVersion = str(int(line[line.index("=")+1:]))
            elif line.startswith("ModelName"):
                modelName = line[line.index("=")+1:].strip()
            elif line.startswith("ModelType"):
                modelType = str(int(line[line.index("=")+1:]))
            elif line.startswith("ModelCreateType"):
                modelCreateType = str(int(line[line.index("=")+1:]))
            elif line.startswith("BaseModelID"):
                baseModelID = str(int(line[line.index("=")+1:]))
            elif line.startswith("ModelLearnType"):
                modelLearnType = str(int(line[line.index("=")+1:]))
            elif line.startswith("LearningType"):
                learningType = str(int(line[line.index("=")+1:]))
            elif line.startswith("CreateTiming"):
                modelCreateTiming = str(int(line[line.index("=")+1:]))
            elif line.startswith("CreateTimeUTC"):
                modelCreateTimeUTC = str(int(line[line.index("=")+1:]))
            elif line.startswith("CreateTime"):
                modelCreateTime = line[line.index("=")+1:].strip()
            elif line.startswith("Comment"):
                modelComment = line[line.index("=")+1:].strip()
            elif line.startswith("CreateUserName"):
                createUserName = line[line.index("=")+1:].strip()
            elif line.startswith("ThresholdEither"):
                thresholdEither = str(int(line[line.index("=")+1:]) / 100.0)
            elif line.startswith("ThresholdBoth"):
                thresholdBoth = str(int(line[line.index("=")+1:]) / 100.0)
            elif line.startswith("AnalysisInterval"):
                analysisInterval = str(int(line[line.index("=")+1:]))
            elif line.startswith("SignificantFigures"):
                significantFigures = str(int(line[line.index("=")+1:]))
            elif line.startswith("NegativeInvariantsHolds"):
                negativeInvariantsHolds = str(int(line[line.index("=")+1:]))
            elif line.startswith("CalcParamN"):
                calcParamN = str(int(line[line.index("=")+1:]))
            elif line.startswith("CalcParamM"):
                calcParamM = str(int(line[line.index("=")+1:]))
            elif line.startswith("CalcParamK"):
                calcParamK = str(int(line[line.index("=")+1:]))
            elif line.startswith("Severity"):
                severity = str(int(line[line.index("=")+1:]))
            elif line.startswith("ExcludeAutoRegressive"):
                excludeAutoRegressive = str(int(line[line.index("=")+1:]))
            elif line.startswith("FitnessThreshold"):
                fitnessThreshold = str(int(line[line.index("=")+1:]))
            elif line.startswith("AutoRegressiveRange"):
                autoRegressiveRange = str(int(line[line.index("=")+1:]))
            elif line.startswith("AutoAnalysisUpdateFlag"):
                autoAnalysisUpdateFlag = str(int(line[line.index("=")+1:]))
            elif line.startswith("TotalStartTimeUTC"):
                totalStartTimeUTC = str(int(line[line.index("=")+1:]))
            elif line.startswith("TotalEndTimeUTC"):
                totalEndTimeUTC = str(int(line[line.index("=")+1:]))
            elif line.startswith("AccumulationID"):
                accumulationID = str(int(line[line.index("=")+1:]))

    #---------------------------------------------------------------------------
    # <id>.csv
    #---------------------------------------------------------------------------
    metric_id_name_dict = dict()
    # with codecs.open(os.path.join(base_dir, "%s.csv" % file_id), "r", "utf-8") as f:
    with codecs.open("%s/%s.csv" % (base_dir, file_id), "r", "utf-8") as f:
        version = f.readline().strip();
        f.readline();
        line3s = f.readline().split(',');
        modelName         = line3s[0].strip('"');
        modelType         = line3s[1].strip('"');
        modelCreateType   = line3s[2].strip('"');
        modelComment      = line3s[3].strip('"');
        createUserName    = line3s[4].strip('"');
        modelRegisterDate = line3s[5].strip('"');
        modelCreateTiming = line3s[6].strip('"');
        modelCreateTimeUTC  = line3s[7].strip('"');
        modelCreateTime   = line3s[8].strip('"');
        analysisInterval  = line3s[9].strip('"');
        invariantCount    = line3s[10].strip('"');
        # [11] def file name
        # [12] dat file name
        infoCount         = line3s[13].strip('"');
        metricCount       = line3s[14].strip('"');
        groupCount        = line3s[15].strip('"');
        ex_metricCount    = line3s[16].strip('"');
        ex_groupCount     = line3s[17].strip().strip('"');
        f.readline();

        xml_root.AddModelInfoNode(modelName, modelType,modelCreateType,
                                  baseModelID, modelLearnType, learningType,
                                  modelCreateTiming, modelCreateTimeUTC, modelCreateTime,
                                  modelRegisterDate,
                                  modelComment, createUserName,
                                  thresholdEither, thresholdBoth,
                                  analysisInterval, significantFigures, negativeInvariantsHolds,
                                  calcParamN, calcParamM, calcParamK,
                                  severity,
                                  metricCount, groupCount, ex_metricCount, ex_groupCount,
                                  excludeAutoRegressive, fitnessThreshold,
                                  autoRegressiveRange, autoAnalysisUpdateFlag,
                                  totalStartTimeUTC, totalEndTimeUTC, accumulationID,
                                  invariantCount, infoCount);

        totalStartTimeUTC = 9999999999999
        totalEndTimeUTC = 0
        for line in f:
            line = line.strip()
            if not line: # 空行
                break
            line_tm = line.split(',')
            start_time_utc = line_tm[0].strip('"')
            end_time_utc   = line_tm[3].strip('"')
            xml_root.AddModelMeasureInfoNode(start_time_utc, end_time_utc)

        # metric list
        metricID = ""
        for line in f:
            line = line.strip()
            if not line: # 空行
                break
            splitted = list(csv.reader([line]))[0]
            metric_id_name_dict[int(splitted[0])] = splitted[1]
            metricID += splitted[1] + ","
        metricID = metricID.rstrip(',')

        # group list
        metricGroupID = ""
        for line in f:
            line = line.strip()
            if not line: # 空行
                break
            splitted = list(csv.reader([line]))[0]
            metricGroupID += splitted[1] + ","
        metricGroupID = metricGroupID.rstrip(',')

        # exclude metric list
        exMetricID = ""
        for line in f:
            line = line.strip()
            if not line: # 空行
                break
            splitted = list(csv.reader([line]))[0]
            metric_id_name_dict[int(splitted[0])] = splitted[1]
            exMetricID += splitted[1] + ","
        exMetricID = exMetricID.rstrip(',')

        # exclide group list
        exMetricGroupID = ""
        for line in f:
            line = line.strip()
            if not line: # 空行
                break
            splitted = list(csv.reader([line]))[0]
            exMetricGroupID += splitted[1] + ","
        exMetricGroupID = exMetricGroupID.rstrip(',')

        xml_root.AddModelMetricInfoNode(metricID, metricGroupID, exMetricID, exMetricGroupID)


    #---------------------------------------------------------------------------
    # <id>_data.dat
    #---------------------------------------------------------------------------
    # with open(os.path.join(base_dir, "%s_data.dat" % file_id), "rb") as f:
    with open("%s/%s_data.dat" % (base_dir, file_id), "rb") as f:

        buf = f.read(356)     # 4 + 24 + 24 + 4 + 4 + 257 +(3)+ 4 + 8 + 8 + 4 + 4 + 4 + 4
        unpacked = struct.unpack("<I24s24sII257s3sIQQII", buf[0:348])
        fileVersion = unpacked[0]
        engineModelID = unpacked[1].decode().strip('\0')
        originalEngineModelID = unpacked[2].decode().strip('\0')
        modelVersion = unpacked[3]
        modelType = unpacked[4]
        modelGroupName = unpacked[5].decode().strip('\0')
        analysisInterval = unpacked[7]
        totalCount = unpacked[8]
        invariantsCount = unpacked[9]
        dim = unpacked[10]
        delay = unpacked[11]

        xml_root.AddInfoNode(fileVersion,
                    engineModelID, originalEngineModelID,
                    modelVersion, modelType, modelGroupName,
                    analysisInterval,
                    totalCount, invariantsCount,
                    dim, delay)

        inv_size = 8 + 8 + 4 + 4 + 4 + 4 + 4 + 4 + (4 * dim)
        def gen():
            while True:
                buf = f.read(inv_size)
                if buf:
                    yield buf
                else:
                    break

        for buf in gen():
            unpacked = struct.unpack("<QQIIIIff", buf[0:40])
            isfiltered = unpacked[2]
            n = unpacked[3]
            m = unpacked[4]
            k = unpacked[5]
            fitness = unpacked[6]
            threshold = unpacked[7]
            theta_list = struct.unpack("<" + "f"*dim, buf[40:])

            xml_root.AddInvariantNode(metric_id_name_dict[unpacked[0]],              # uname
                                      metric_id_name_dict[unpacked[1]],              # yname,
                                      isfiltered,                                      # filtered
                                      n,                                             # n
                                      m,                                             # m
                                      k,                                             # k
                                      fitness,                                       # fitness
                                      threshold,                                     # threshold
                                      ",".join(str(x) for x in theta_list[0:n+m+2]), # theta,
                                      )

    # print(xml_root.ToString())

    xml_file = dirname + ".xml"    
    with open(xml_file, 'w') as f:
        f.write(xml_root.ToString())

    return xml_root

################################################################################
def ExtractMdlFile(mdlFile):
    with zipfile.ZipFile(mdlFile) as existing_zip:
        existing_zip.extractall()

################################################################################
if __name__ == "__main__":

    args = sys.argv

    if len(args) < 3:
        print("parameter error!")
        sys.exit(1)

    filepath = args[1]
    outfile = args[2]

    filename = os.path.basename(filepath)
    file_id = os.path.splitext(filename)[0]
#    ExtractMdlFile(filepath)

    model_xml = Mdl2Xml(file_id, filepath)

    fo = open(outfile, 'w')
    sys.stdout = fo
    print(model_xml.ToString())
    fo.close()

    shutil.rmtree(file_id)
