# -*- coding: utf-8 -*-
import urllib.request, urllib.parse, urllib.error;
import urllib.request, urllib.error, urllib.parse;
import json;
import datetime;
import contextlib;
import time;

SiatMgrVersion = "1.3";
ForceNoProxy = True;


if ForceNoProxy:
    # To force not to use proxy
    proxy_handler = urllib.request.ProxyHandler({});
    opener = urllib.request.build_opener(proxy_handler);
    urllib.request.install_opener(opener);


class SiatMgrConnector_v1_0(object):
    Port = 12639;
    Version = "1.0";

    def __init__(self, host):
        self.Host = host;
    
    def BuildURL(self, endpoint):
        return "http://%s:%d%s" % (self.Host, self.Port, endpoint);

    def SendPutRequest(self, endpoint, data):
        with contextlib.closing(urllib.request.build_opener(urllib.request.HTTPHandler)) as opener:
            request = urllib.request.Request(self.BuildURL(endpoint), json.dumps(data));
            request.add_header("Content-Type", "application/json");
            request.get_method = lambda: "PUT";
            f = opener.open(request);
            return json.load(f);

    def SendPostRequest(self, endpoint, data):
        request = urllib.request.Request(self.BuildURL(endpoint));
        request.add_header("Content-Type", "application/json");
        with contextlib.closing(urllib.request.urlopen(request, json.dumps(data))) as f:
            data = json.load(f);
            return data;

    def SendDeleteRequest(self, endpoint, data=None):
        with contextlib.closing(urllib.request.build_opener(urllib.request.HTTPHandler)) as opener:
            request = urllib.request.Request(self.BuildURL(endpoint), data);
            request.add_header("Content-Type", "application/json");
            request.get_method = lambda: "DELETE";
            f = opener.open(request);
            return json.load(f);

    def SendGetRequest(self, endpoint):
        f = urllib.request.urlopen(self.BuildURL(endpoint))
        with contextlib.closing(urllib.request.urlopen(self.BuildURL(endpoint))) as f:
            data = json.loads(f.read().decode('utf-8'));
            return data;

    def WaitForImportFinish(self, req_id):
        while True:
            st = self.GetRequestStatus(req_id);
            res_code = st["ResponseCode"];
            if "Progress" not in st:
                if res_code != 0:
                    raise Exception("Import measure file error %d" % (res_code));
                return;
            time.sleep(1);

    def GetModelList(self):
        endpoint = "/v1/plantmgrapi/models";
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrModelList"];

    def GetModelInfo(self, id):
        endpoint = "/v1/plantmgrapi/models/%d" % id;
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrModelInfo"];

    def GetModelDetail(self, id):
        # Model Detailで返されるinvariantsの個数をmaxに変更
        original_num = self.GetOptions()["AnomalyResultCount"];
        self.SetNumInvariantsOfModelDetail(0x7fffffff); # max int(32bit)
        
        endpoint = "/v1/plantmgrapi/models/data/%d" % id;
        data = self.SendGetRequest(endpoint);
        
        # 変更した数をもとに戻しておく
        self.SetNumInvariantsOfModelDetail(original_num);
        return data["PlantMgrModelData"];

    def CreateModel(self, name, start_time_utc, end_time_utc, analysis_interval,
                    metric_id_list, metric_group_id_list = None,
                    param_n = 1, param_m = 1, param_k = 0,
                    model_type = 0, create_timing = 0, create_time = None,
                    comment = "", threshold_either = 70, threshold_both = 50,
                    start_time = "0", end_time = "0", significant_figures = 5,
                    negative_invariants_holds = 0, severity = 1):
        endpoint = "/v1/plantmgrapi/models";
        if metric_group_id_list is None:
            metric_group_id_list = [];
        if create_time is None:
            create_time = datetime.datetime.now();
        req = {"PlantMgrModelInfo":
                   {"ModelName": name,
                    "ModelType": model_type,
                    "ModelCreateTiming": create_timing,
                    "ModelCreateTime": create_time.strftime("%Y/%m/%d %H:%M:%S"),
                    "ModelComment": comment,
                    "CreateUserName": "SiatMgrConnector",
                    "ThresholdEither": threshold_either,
                    "ThresholdBoth": threshold_both,
                    "StartTimeUTC": start_time_utc,
                    "StartTime": start_time,
                    "EndTimeUTC": end_time_utc,
                    "EndTime": end_time,
                    "AnalysisInterval": analysis_interval,
                    "SignificantFigures": significant_figures,
                    "NegativeInvariantsHolds": negative_invariants_holds,
                    "CalcParamN": param_n,
                    "CalcParamM": param_m,
                    "CalcParamK": param_k,
                    "Severity": severity,
                    "MetricID": metric_id_list,
                    "MetricGroupID": metric_group_id_list
                    }};
        return self.SendPostRequest(endpoint, req);
        

    def GetMetricsList(self):
        endpoint = "/v1/plantmgrapi/metrics?mode=1";
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrMetricInfo"];

    def GetMetricsInfo(self, id):
        endpoint = "/v1/plantmgrapi/metrics/%d" % id;
        with contextlib.closing(urllib.request.urlopen(self.BuildURL(endpoint))) as f:
            return f.read();

    def PostMetricItems(self, data):
        endpoint = "/v1/plantmgrapi/metrics";
        req = {"MetricCount": len(data),
               "PlantMgrAddMetricInfo": data
               };
        ret = self.SendPostRequest(endpoint, req);
        return ret;

    def UpdateMetricItem(self, id, data):
        endpoint = "/v1/plantmgrapi/metrics/%d" % id;
        req = {"PlantMgrMetricInfo": data};
        ret = self.SendPutRequest(endpoint, req);
        return ret;

    def DeleteMetricItems(self, id_list):
        self.DeleteItems(id_list);

    def SetMeasuresData(self, id, data_list):
        endpoint = "/v1/plantmgrapi/measures/modify/%d" % id;
        req = {"MetricDataCount": len(data_list),
               "PlantMgrMetricDataList": data_list};
        self.SendPutRequest(endpoint, req);

    def RequestImportMeasureFile(self, filename):
        endpoint = "/v1/plantmgrapi/measures";
        req = {"PlantMgrMeasureFileName": filename};
        return self.SendPostRequest(endpoint, req);

    def GetMeasuresPeriod(self, id):
        endpoint = "/v1/plantmgrapi/measures/period?metricid=%d" % id;
        data = self.SendGetRequest(endpoint);
        return data;

    def GetGroupList(self):
        endpoint = "/v1/plantmgrapi/groupinfo";
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrGroupInfo"];

    def GetAnalysisList(self, analysis_type = None):
        """type: 0 is ondemand, 1 is realtime, 2 is simulation"""
        endpoint = "/v1/plantmgrapi/analyses/list"
        if analysis_type is not None:
            endpoint = endpoint + ("?analysistype=%d" % analysis_type);
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrAnalysisList"];

    def RequestAnalysis(self, type, name, model_schedule_list, 
                        threshold_overhead,
                        detection, recovery,
                        control_span, 
                        start_time = None, end_time = None,
                        ranking = 0,
                        comment = ""):
        endpoint = "/v1/plantmgrapi/analyses";
        req = {"AnalyzeType": type,
               "AnalysisName": name,
               "UserName": "SiatMgrConnector",
               "Comment": comment,
               "ModelCount": len(model_schedule_list),
               "PlantMgrModelScheduleInfo": model_schedule_list,
               "PlantMgrAnalyzeReq": 
               {
                "ThresholdOverhead": threshold_overhead,
                "Detection": detection,
                "Recovery": recovery,
                "ControlSpan": control_span,
                "Ranking": ranking
                }};
        if type != 1: # リアルタイム分析以外の場合
            req["PlantMgrAnalyzeReq"]["StartTime"] = start_time;
            req["PlantMgrAnalyzeReq"]["EndTime"] = end_time;
        return self.SendPostRequest(endpoint, req);

    def GetAnomalyScore(self, analysis_id, base_time, data_range):
        """base_time = start time(utc) in milliseconds, data_range = range in seconds"""
        endpoint = "/v1/plantmgrapi/results/anomalyscore/%d" % int(analysis_id);
        req = {"PlantMgrAnomalyDataReq":
                   {"BaseTime": base_time,
                    "Range": data_range
                    }};
        res = self.SendPostRequest(endpoint, req);
        return res["PlantMgrAnomalyData"];

    def GetIncidentList(self, analysis_id=None):
        endpoint = "/v1/plantmgrapi/incident?mode=0";
        if analysis_id:
            endpoint += "&analysistype=0&analysisid=%d" % (int(analysis_id));
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrIncidentInfo"];

    def GetAnomalyMap(self, analysis_id, analysis_time):
        """analysis_time = time(utc) in milliseconds"""
        endpoint = "/v1/plantmgrapi/results/anomalymap/%d" % analysis_id;
        req = {"PlantMgrAnomalyMapDataReq":
               {"AnalysisTime": analysis_time}};
        res = self.SendPostRequest(endpoint, req);
        return res;

    def GetResidualGraph(self, analysis_id, model_id,
                         base_time, time_range,
                         input_metric_id, output_metric_id):
        endpoint = "/v1/plantmgrapi/results/residual/%d" % analysis_id;
        req = {"PlantMgrResidualDataReq":
                   { "ModelID": model_id,
                     "BaseTime": base_time,
                     "Range": time_range,
                     "InputMetricID": input_metric_id,
                     "OutputMetricID": output_metric_id
                }};
        res = self.SendPostRequest(endpoint, req);
        return res;

    def GetOptions(self):
        endpoint = "/v1/plantmgrapi/options";
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrOptionInfo"];

    def SetOptions(self, data):
        endpoint = "/v1/plantmgrapi/options";
        return self.SendPutRequest(endpoint, data);

    def SetNumInvariantsOfModelDetail(self, num):
        data = self.GetOptions();
        data["AnomalyResultCount"] = num;
        data = { "PlantMgrOptionInfo": data};
        self.SetOptions(data);
        
    def GetProgress(self, id):
        endpoint = "/v1/plantmgrapi/requests/%d" % id;
        data = self.SendGetRequest(endpoint);
        response_code = data["ResponseCode"];
        if response_code == 0:
            return 100;
        elif response_code == 1:
            return data["Progress"];
        return -1;

    def GetRequestStatus(self, id):
        endpoint = "/v1/plantmgrapi/requests/%d" % id;
        data = self.SendGetRequest(endpoint);
        return data;

    def DeleteItem(self, id):
        endpoint = "/v1/plantmgrapi/requests/delete/";
        f = self.SendDeleteRequest(endpoint + str(id));
        return f;

    def DeleteItems(self, id_list):
        for i in id_list:
            self.DeleteItem(i);


class SiatMgrConnector_v1_2(SiatMgrConnector_v1_0):
    Version = "1.2";

    def __init__(self, host):
        SiatMgrConnector_v1_0.__init__(self, host);
        
    def CreateModel(self, name, start_time_utc, end_time_utc, analysis_interval,
                    metric_id_list, metric_group_id_list = None,
                    param_n = 1, param_m = 1, param_k = 0,
                    model_type = 0, create_timing = 0, create_time = None,
                    comment = "", threshold_either = 70, threshold_both = 50,
                    start_time = "0", end_time = "0", significant_figures = 5,
                    negative_invariants_holds = 0, severity = 1,
                    exclude_ar = 0, ar_fitness_thresh = 70, ar_range = 3):
        endpoint = "/v1/plantmgrapi/models";
        if metric_group_id_list is None:
            metric_group_id_list = [];
        if create_time is None:
            create_time = datetime.datetime.now();
        req = {"PlantMgrModelInfo":
                   {"ModelName": name,
                    "ModelType": model_type,
                    "ModelCreateTiming": create_timing,
                    "ModelCreateTime": create_time.strftime("%Y/%m/%d %H:%M:%S"),
                    "ModelComment": comment,
                    "CreateUserName": "SiatMgrConnector",
                    "ThresholdEither": threshold_either,
                    "ThresholdBoth": threshold_both,
                    "StartTimeUTC": start_time_utc,
                    "StartTime": start_time,
                    "EndTimeUTC": end_time_utc,
                    "EndTime": end_time,
                    "AnalysisInterval": analysis_interval,
                    "SignificantFigures": significant_figures,
                    "NegativeInvariantsHolds": negative_invariants_holds,
                    "CalcParamN": param_n,
                    "CalcParamM": param_m,
                    "CalcParamK": param_k,
                    "Severity": severity,
                    "MetricID": metric_id_list,
                    "MetricGroupID": metric_group_id_list,
                    "ExcludeAutoRegressive": exclude_ar,
                    "FitnessThreshold": ar_fitness_thresh,
                    "AutoRegressiveRange": ar_range
                    }};
        return self.SendPostRequest(endpoint, req);

    def RequestAnalysis(self, type, name, model_schedule_list, 
                        threshold_overhead,
                        detection, recovery,
                        control_span, 
                        start_time = None, end_time = None,
                        ranking = 0,
                        comment = "",
                        exclude_constant_broken = 0,
                        continuous_period = 1,
                        sampling_points = 1,
                        threshold_constant_broken = 1):
        endpoint = "/v1/plantmgrapi/analyses";
        req = {"AnalyzeType": type,
               "AnalysisName": name,
               "UserName": "SiatMgrConnector",
               "Comment": comment,
               "ModelCount": len(model_schedule_list),
               "PlantMgrModelScheduleInfo": model_schedule_list,
               "PlantMgrAnalyzeReq": 
               {
                "ThresholdOverhead": threshold_overhead,
                "Detection": detection,
                "Recovery": recovery,
                "ControlSpan": control_span,
                "Ranking": ranking,
                "ExcludeConstantlyBroken": exclude_constant_broken
                }};
        if type != 1: # リアルタイム分析以外の場合
            req["PlantMgrAnalyzeReq"]["StartTime"] = start_time;
            req["PlantMgrAnalyzeReq"]["EndTime"] = end_time;
        if exclude_constant_broken: # 定常相関破壊を除外する場合
            req["PlantMgrAnalyzeReq"]["ContinuousPeriod"] = continuous_period;
            req["PlantMgrAnalyzeReq"]["SamplingPoints"] = sampling_points;
            req["PlantMgrAnalyzeReq"]["ThresholdConstantlyBroken"] = threshold_constant_broken;
        return self.SendPostRequest(endpoint, req);


    def GetAnomalyMap(self, analysis_id, analysis_time, 
                      anomaly_score_count=None,
                      anomaly_rscore_count=None,
                      constantly_broken_flag=None):
        """analysis_time = time(utc) in milliseconds"""
        endpoint = "/v1/plantmgrapi/results/anomalymap/%d" % analysis_id;
        req = {"PlantMgrAnomalyMapDataReq":
               {"AnalysisTime": analysis_time}};
        if anomaly_score_count is not None:
            req["PlantMgrAnomalyMapDataReq"]["AnomalyScoreCount"] = anomaly_score_count;
        if anomaly_rscore_count is not None:
            req["PlantMgrAnomalyMapDataReq"]["AnomalyRscoreCount"] = anomaly_rscore_count;
        if constantly_broken_flag is not None:
            req["PlantMgrAnomalyMapDataReq"]["ConstantlyBrokenFlag"] = constantly_broken_flag;
        res = self.SendPostRequest(endpoint, req);
        return res;


    def GetModelDetail(self, id, start_point=None,
                       display_count=None):
        endpoint = "/v1/plantmgrapi/models/data/%d" % id;
        if not start_point:
            start_point = 1;
        if not display_count:
            display_count = 0x7fffffff;
        endpoint += "?StartPoint=%d&DisplayCount=%d" % (start_point, display_count);
        data = self.SendGetRequest(endpoint);
        return data["PlantMgrModelData"];


class SiatMgrConnector_v1_3(SiatMgrConnector_v1_2):
    Version = "1.3";

    def __init__(self, host):
        SiatMgrConnector_v1_2.__init__(self, host);

    def CreateModel(self, name, start_time_utc, end_time_utc, analysis_interval,
                    metric_id_list, metric_group_id_list = None,
                    param_n = 1, param_m = 1, param_k = 0,
                    model_type = 0, create_timing = 0, create_time = None,
                    comment = "", threshold_either = 70, threshold_both = 50,
                    start_time = "0", end_time = "0", significant_figures = 5,
                    negative_invariants_holds = 0, severity = 1,
                    exclude_ar = 0, ar_fitness_thresh = 70, ar_range = 3):
        if create_timing == 0:
            return self.CreateModelImmediate(name, [(start_time_utc, end_time_utc)], analysis_interval,
                                      metric_id_list, metric_group_id_list,
                                      param_n, param_m, param_k,
                                      model_type, comment,
                                      threshold_either, threshold_both,
                                      significant_figures, negative_invariants_holds,
                                      severity,
                                      exclude_ar, ar_fitness_thresh, ar_range);
        else:
            raise Exception("Not supported");

    def CreateModelImmediate(self, name, start_end_pair_list,
                             analysis_interval, metric_id_list, metric_group_id_list = None,
                             param_n = 1, param_m = 1, param_k = 0,
                             model_type = 0, comment = "", 
                             threshold_either = 70, threshold_both = 50,
                             significant_figures = 14, negative_invariants_holds = 0,
                             severity = 1, 
                             exclude_ar = 0, ar_fitness_thresh = 70, ar_range = 3,
                             accumulation_id = 0):
        endpoint = "/v1/plantmgrapi/models";
        if metric_group_id_list is None:
            metric_group_id_list = [];
        req = {"PlantMgrModelInfo":
                   {"ModelName": name,
                    "ModelType": model_type,
                    "ModelCreateTiming": 0, # 即時
                    "ModelComment": comment,
                    "CreateUserName": "SiatMgrConnector",
                    "ThresholdEither": threshold_either,
                    "ThresholdBoth": threshold_both,
                    "AnalysisInterval": analysis_interval,
                    "SignificantFigures": significant_figures,
                    "NegativeInvariantsHolds": negative_invariants_holds,
                    "CalcParamN": param_n,
                    "CalcParamM": param_m,
                    "CalcParamK": param_k,
                    "Severity": severity,
                    "MetricID": metric_id_list,
                    "MetricGroupID": metric_group_id_list,
                    "ExcludeAutoRegressive": exclude_ar,
                    "FitnessThreshold": ar_fitness_thresh,
                    "AutoRegressiveRange": ar_range,
                    "AccumulationID": accumulation_id,
                    "MeasureInfoCount": len(start_end_pair_list),
                    "MeasureInfo":
                        [{"StartTimeUTC":p[0], 
                          "EndTimeUTC":p[1],
                          "StartTime": "",
                          "EndTime": "",
                          "StartPrevDayFlag": 0,
                          "EndPrevDayFlag": 0} for p in start_end_pair_list],
                    "ModelCreateTimeUTC": 0,
                    "ModelCreateTime": ""
                    }};
        return self.SendPostRequest(endpoint, req);


SiatMgrConnector = SiatMgrConnector_v1_0;

if SiatMgrVersion == "1.0":
    pass;
elif SiatMgrVersion == "1.2":
    SiatMgrConnector = SiatMgrConnector_v1_2;
elif SiatMgrVersion == "1.3":
    SiatMgrConnector = SiatMgrConnector_v1_3;

if __name__ == "__main__":
    pass;
