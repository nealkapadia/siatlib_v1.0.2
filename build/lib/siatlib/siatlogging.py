import logging
import time
from datetime import datetime

log = logging.getLogger(__name__)



class SiatLogging:

    def __init__(self, filename, loglevel=logging.DEBUG):
        self.enable_logging(loglevel=loglevel, filename=filename)

    def enable_logging(self, loglevel, filename):
        debug_file_name = f"{filename}_debuglogs_{datetime.now().strftime('%Y_%m_%d_%H_%M_%S.logs')}"

        #formatter = '%(asctime)s:%(filename)s:%(funcName)s:%(lineno)d:%(levelname)-8s: %(message)s'
        formatter = '%(asctime)s:%(levelname)-8s: %(message)s'

        #Initialize the logging module
        logging.basicConfig(level=loglevel,
                            filename=debug_file_name,
                            filemode='w',
                            format=formatter) # include timestamp

        logging.propagate = False


