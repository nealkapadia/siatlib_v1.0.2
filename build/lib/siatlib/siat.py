"""Used to Interact with the SIAT using the RESTAPI.
Fitness, Predict, Metric Import and Model Creation/Updation are
support via this module.

"""
import time
import datetime
import pprint
import pandas as pd
import requests
import logging
import os
import shutil
import __main__
import sys
from siatlib import siatutils
import numpy as np

import socket
import posixpath


class Siat():
    r'''Used to Interact with SIAT using REST APIs

    Attributes
    ----------
    params             : dict
                         The Parameters present in input.params file parsed as dict

    kwargs             : dict
                         The Keyword Args as passed while instantiating the class

    model_inputs       : dict
                         Will contain the arguments passed while instantiating the class
                         except MetricData

    fit_data           : pandas.DataFrame
                         Contains the MetricData keyword argument content in pandas
                         format

    siat_status        : int
                         Will be set to 0 if siat status is Down and 1 if its Up.
                         Once the class is instantiated it does a get request and
                         finds the status.

    siat_tzone         : str
                         This will contain the timezone of SIAT. Once the class
                         is instantiated it does a get request and finds out the 
                         status as well as the timezone of SIAT.

    group_mapping      : dict
                         Dict with Key being the Group Name and Value being the 
                         Group ID of the newly registered Group.

    group_name         : str
                         Contains the Group Name that is getting registered

    group_details      : dict
                         json input for POST request for registering Groups.

    metric_names       : list
                         Its a list of metric names that are going to be created
                         in SIAT.

    metric_details     : dict
                         json input for POST request for regsitering Metrics.

    metric_mapping     : dict
                         Dict with Key being the Metric Name and Value being the
                         Metric ID of the newly registered Metrics.

    model_mapping      : dict
                         Dict with the Key being the Model Name and Value being the
                         Model ID of the newly created/registered Model.

    analysis_mapping   : dict
                         Dict with the Key being the Analysis Name and Value being the
                         Analysis ID of the Analysis Request.

    import_file_name   : str
                         Contains the filename that needs to be imported by SIAT

    predict_inputs     : dict
                         Contains the parameters passed to the predict method

    analyze_inputs     : dict
                         Contains the json which will be used as input to POST the
                         analysis request

    log                : logging.Logger
                         Has the Logger which is used for dumping log infoAnalysisInterval

    Examples
    --------
    To instantiate the Class you can do it as simple as:

    >>> siatObj = Siat()
    '''

    def __init__(self):
        #Check which file is invoking this lib.
        #Check for siat.params in that folder.
        self.log = logging.getLogger(__name__)
        try:
            main_file = os.path.abspath(sys.modules['__main__'].__file__)
        except:
            self.log.error("You get this error only in interactive mode.", exc_info=True)
            dummy_file = input("Please Enter the Directory where siat.params needs to be created or is currently located: ")
            main_file = os.path.join(dummy_file, 'dummy.py')

        cwd = os.path.dirname(main_file)
        params_file = os.path.join(cwd, 'siat.params')
        self.utils = siatutils.Utils(params_file)
        utils = self.utils
        self.params = utils.input_params
        self.siat_status = 0
        self.siat_status_check()
        self.metric_names = []
        self.group_name = ''

    def siat_status_check(self):
        r'''Used to Check if the SIAT is up and running  and also finds 
        out the timezone of the SIAT.

        Parameters
        ----------
        None

        Returns
        -------
        1   :   int
                SIAT Status is UP and TimeZone is sucessfully parsed.
        0   :   int
                SIAT Status is Down

        '''
        self.log.info('Checking SIAT Status')
        utils = self.utils
        req_url = utils.build_url(key='status-url')

        res = utils.send_request(url=req_url, req_type='get')

        retval = utils.check_http_status_code(res, requests.codes.ok)

        if (retval == 1):
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            self.log.info(f'SIAT Status is UP')
            try:
                self.siat_tzone = res.json()['PlantMgrStatusInfo']['TimeZone']
                self.siat_status = 1
            except:
                self.log.error("Response is empty", exc_info=True)
                self.siat_status = 0
        else:
            retval = 0
            self.log.error(f'SIAT Status is NOT UP')
            self.log.error(f'res.text')

    def _create_group_dict(self, count):
        r'''An Internal Method used to create a Group Name and
        also create the json input dictionary that SIAT understands
        for a POST operation

        Notes
        -----
        **Does NOT return any value. Just creates a method variable group_details
        and group_name. group_details is the json input for post.**

        '''
        dummy_lst = []
        for x in list(range(count)):
            seconds = time.time()
            group_name = str(int(seconds))
            self.group_name = group_name
            dict = {"GroupPath" : "/" + group_name , "GroupComment" : "Group_" + group_name}
            dummy_lst.append(dict)

        self.group_details = {"GroupCount" : count, "PlantMgrAddGroupInfo" : dummy_lst}

    def _register_metric_group(self):
        r'''Creates a Metric Group/Group in SIAT using POST request.

        Parameters
        ----------
        None

        Returns
        -------
        1 : int
            Successfully created the Metric Group in SIAT

        0 : int
            Metric Group Creation Failed.

        Notes
        -----
        **On the event of Metric Group Creation failure check the logs to find
        the url used in post and also the http status code and
        SIAT response code which gets dumped into the logs.**

        '''
        self.log.info('Registering Metric Group')
        utils = self.utils
        req_url = utils.build_url(key='group-url')
        #group_name = self.group_dict['PlantMgrAddGroupInfo'][0]['GroupPath']
        self._create_group_dict(1)
        group_name = self.group_name
        group_dict = self.group_details

        res = utils.send_request(url=req_url, req_type='post', post_dict=group_dict)

        retval = utils.check_http_status_code(res, requests.codes.ok)


        if (retval == 1):
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            self.log.info(f'Created Group {group_name} Successfully')
        else:
            retval = 0
            self.log.error(f'Creation of group {group_name} failed')
            utils._lookup_response(res, req_type = 'RegisterGroup')
            return retval

        #Create a Group Mapping of the newly created Group
        retval = self._get_metric_group_id()
        return retval

    def _get_metric_group_id(self):
        r'''This is called by _register_metric_group method to do a 
        group name to id mapping.
        
        It creates an instance variable group_mapping which is a 
        dict with the key as group name and value as group id on 
        successful retrieval of group-id
        
        
        Returns
        -------
        0             : int 
                        Failed to Map the Group id to Group Name
        1             : int 
                        Successfully mapped Group id to Group Name
        '''
        self.log.info(f'Fetching Groupid for newly created group {self.group_name}')

        utils = self.utils
        req_url = utils.build_url(key='group-url')
        req_url = f'{req_url}?groupname={self.group_name}'
        res = utils.send_request(url=req_url, req_type='get')
        metric_group_name = self.group_name

        retval = utils.check_http_status_code(res,requests.codes.ok)

        if (retval == 1):
            #Correct HTTP Status Code
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)

            if(retval == 1):
               #Correct SIAT Response Code
               try:
                   if(res.json()['GroupCount'] == 1):
                       group_id = res.json()['PlantMgrGroupInfo'][0]['GroupID']
                       self.log.info(f'Retrieved Group id {group_id} for Group {metric_group_name} successfully')
               except:
                   self.log.error("No Response Found", exc_info=True)
                   retval = 0
            else:
                #Wrong Response Code from SIAT
                self.log.error(f'Unable to retrieve Group id for Group {metric_group_name}')
                retval = 0
                utils._lookup_response(res, req_type = 'GetGroupID')
                return retval
        else:
            #Wrong HTTP Status Code
            retval = 0
            self.log.error(f'Unable to retrieve Group id for Group {metric_group_name}')
            self.log.error(res.json())
            return retval
        self.group_mapping = {metric_group_name : group_id}
        return retval

    def _parse_pandas_frame(self, data, group_name):
        r'''This method parses the column names from the pandas dataframe.

           Columns 2 till last column is assumed to be metric names
           and Group name is prepended to the metric name

           Parameters
           ----------
           data, group_name

           Returns
           -------
           List of Metric Names

           Notes
           -----
           **Column 1 is TimeStamp. Column 2:N is Sensor Names.
           metric_names variable will contain the names of the
           metrics that are going to be created in SIAT**

           Raises
           ------
           TypeError
               Raised if MetricData is NOT a Pandas DataFrame Format
           AttributeError
               Raised if Metric Group is NOT registered

           Warnings 
           --------
           **Needs group_mapping instance variable to be set
           before we call this method.(Register Metric Group
           first and then try to register Metrics)**

        '''
        #For Test purpose we are reading the csv file as data frame
        #data = pd.read_csv("sample.csv")
        #data = self.data
        #Get the Sensor names from the data Frame
        try:
            sensor_names = list(data.columns)[1:]
        except:
            self.log.error("Looks like the MetricData that is passed is not a pandas dataframe", exc_info=True)
            raise TypeError
        #Group name is prepended to metric name to get a unique naming convention
        met_list = [f'{group_name}.' + s for s in sensor_names]
        self.metric_names = met_list
        return met_list

    def _create_metric_dict(self, met_names_lst):
        r'''An Internal Method used to create the json input dictionary that 
          SIAT understands for a POST operation to register metrics

          It creates a metrics_details instance variable which will be the
          json input for post operation

          Parameters
          ----------
          None

          Raises
          ------
          AttributeError
              Raised if Metric Group is NOT registered

          Returns
          -------
          None
        '''

        count = len(met_names_lst)
        dummy_lst = []

        try:
            group_id = self.group_mapping[list(self.group_mapping)[0]]
        except:
            self.log.error("No Group Mapping Found, Please register Metric Group First", exc_info=True)
            raise

        for x in met_names_lst:
            dict = {"MetricName" : x , "AggregateType" : 1, "GroupCount" : 1, "GroupID" : [group_id], "StandardName" : x}
            dummy_lst.append(dict)
        self.metric_details = {"MetricCount" : count, "PlantMgrAddMetricInfo" : dummy_lst}

    def _register_metrics_name(self):
        r'''Creates Metric and does a MetricName to ID Mapping if successful.

        Parameters
        ----------
        None


        Returns
        -------
        1 : int
            If Post Request is successful then it returns 1
        0 : int
            If the Post Request fails then it returns 0

        Notes
        -----
        **On the event of Metric Creation failure check the logs to find
        the url used in post and also the http status code and
        SIAT response code which gets dumped into the logs.**

        '''
        self.log.info('Registering Metric Names')
        utils = self.utils
        req_url = utils.build_url(key='metric-url')

        #get_metric_names
        try:
            group_name = self.group_name
        except:
            self.log.error("No Group Name Found, Please register Metric Group First", exc_info=True)
            raise
        met_list = self._parse_pandas_frame(self.fit_data, group_name)
        #Create Dictionary
        self._create_metric_dict(met_list)
        metric_dict = self.metric_details
        task = 'RegisterMetric'
        key = 'RequestID'
        mapping = self._get_metric_id
        err = 'Metric Name Creation Error'

        retval = self.post_and_get_async_result(req_url=req_url, task_dict=metric_dict, task=task,\
                 mapping=mapping, key=key, err=err)

        if retval == 1:
            self.log.info("Metrics Registered Successfully")

        return retval

    def _get_metric_id(self, req):
        r'''This is called by _register_metrics_name method to do a
        metric name to id mapping.
        
        It creates an instance variable metric_mapping which is a 
        dict with the key as metric name
        and value as metric id on successful retrieval of metric-id

        Parameters
        ----------
        req : str
              This is a dummy variable we dont do anything with it.
              Needed for post_and_get_async_result method to do a
              mapping

        Returns
        -------
        0 : int 
            Returns 1 on success.
        1 : int
            Returns 0 on failure to get the metric id
        '''
        self.log.info("Fetching Metric ID for newly created Metrics")
        utils = self.utils
        req_url = utils.build_url(key='metric-url')
        group_id = self.group_mapping[list(self.group_mapping)[0]]
        req_url = f'{req_url}?mode=2&groupid={group_id}'
        res = utils.send_request(url=req_url, req_type='get')

        retval = utils.check_http_status_code(res,requests.codes.ok)
        self.metric_mapping = {}

        if (retval == 1):
            #Correct HTTP Status Code
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            if(retval == 1):
                #Correct SIAT Response Code
                i = 0
                try:
                    metric_list = res.json()['PlantMgrMetricInfo']
                except:
                    self.log.error("Error when checking response", exc_info=True)
                for x in metric_list:
                    try:
                        metric_name = res.json()['PlantMgrMetricInfo'][i]['MetricName']
                        metric_id = res.json()['PlantMgrMetricInfo'][i]['MetricID']
                        self.metric_mapping[metric_name] = metric_id
                        i += 1
                    except:
                        self.log.error("Error when checking response", exc_info=True)

                self.log.info("Metric name and id mapping done successfully")
                self.log.debug(self.metric_mapping)
            else:
                #Wrong Response Code from SIAT
                self.log.error(f'Unable to retrieve Metric id using Group-id {group_id}')
                retval = 0
                self.log.error(res.json())
        else:
            #Wrong HTTP Status Code
            retval = 0
            self.log.error(f'Unable to retrieve Metric id using Group-id {group_id}')
            self.log.error(res.text)
        return retval

    def _import_metric_file(self, data, group_name, **kwargs):
        r'''Convert the pandas Data Frame into SIAT CSV Format and then import
        
        Parameters
        ----------
        data                          : pandas.DataFrame
                                        Pandas DataFrame which contains the Data
        group_name                    : string
                                        Contains the Group Name
        PlantMgrMeasureFileType       : int, optional
                                        Type of Import Metric File
        PlantMgrMeasureMetricNameType : int, optional
                                        Metric Name Type
        PlantMgrMeasureFileName       : str, optional
                                        Import Metric File Name

        Returns
        -------
        1 : On Successful import of the Metric File
        0 : On Failure to import the Metric File

        Notes
        -----
        **On the event of Metric file Import failure,  check the logs to find
        the url used in post and also the http status code and
        SIAT response code which gets dumped into the logs.**

        '''
        self.log.info('Importing Metric File')
        utils = self.utils
        self._prepare_import_file(data, group_name)
        self.import_dict = kwargs
        import_dict = self.import_dict
        req_url = utils.build_url(key='import-metric-url')

        if import_dict.get("PlantMgrMeasureFileType",None) == None:
            import_dict["PlantMgrMeasureFileType"] = 1

        if import_dict.get("PlantMgrMeasureMetricNameType",None) == None:
            import_dict["PlantMgrMeasureMetricNameType"] = 1

        if import_dict.get("PlantMgrMeasureFileName",None) == None:
            import_dict["PlantMgrMeasureFileName"] = self.import_file_name

        env_type=f"{self.params['env-detail']['arch-type']}"
        self.env_type=env_type

        if self.env_type=="0":
            if self._get_platform()=="posixpath":
                loc = f"{self.params['storage-loc']['server-import-file-loc']}/{self.import_file_name}"
            elif self._get_platform()=="windowspath":                
                loc = f"{self.params['storage-loc']['server-import-file-loc']}\{self.import_file_name}"
        else:
            if self._get_platform()=="posixpath":                
                loc = f"{self.params['storage-loc']['client-import-file-loc']}/{self.import_file_name}"
            elif self._get_platform()=="windowspath":                
                loc = f"{self.params['storage-loc']['client-import-file-loc']}\{self.import_file_name}"


        self.log.info(f'Metric File Location is {loc}')

        task = 'ImportMetric'
        key = 'RequestID'
        mapping = self.log.debug
        err = 'Metrics File Import Error'

        retval = self.post_and_get_async_result(req_url=req_url, task_dict=import_dict, task=task,\
                 mapping=mapping, key=key, err=err)

        if retval == 1:
            self.log.info("Metric File Import Successful")

        return retval

    def _prepare_import_file(self, data, group_name):
        r'''Reads the data from pandas dataframe and converts into SIAT supported csv
        file.

        This method renames the columns of the data frame with the new metric names
        and also replaces the first column in the dataframe with the timezone
        of the siat which is present in the siat_tzone variable. The dataframe
        is then converted into a SIAT supported CSV format.

        Parameters
        ----------
        data       : pandas.DataFrame
                     Pandas DataFrame which contains the Data
        group_name : string
                     Contains the Group Name

        Raises
        ------
        IOError
            Raised if the DataFrame is NOT able to be written/converted to a csv file

        Returns
        -------
        None
        '''
        #This is used change the dataFrame to a siat supported csv file
        #below is a patch to test data
        utils = self.utils
        #cols = data.columns[1:]
        #dtype = {}
        #for x in cols:
        #    dtype[x] = str
        #data = pd.read_csv("sample.csv",dtype = dtype)
        #above code will be removed in final version
        self.import_file_name = f'converted{int(time.time())}.csv'


        #If you are Importing for Predict then the Pandas Data Frame
        #is read to get the names.
        #If metric_names list is empty then fit has not been executed
        if not self.metric_names:
            metric_names = self._parse_pandas_frame(data, group_name)
        else:
            metric_names = self.metric_names

        #Get Column Names from Pandas Frame
        #Rename the columns as per the siat format
        columns = {}
        new_names = [f'(SIAT-CSV 1.0) {self.siat_tzone}'] + metric_names
        i = 0
        for x in list(data.columns):
            columns[x] = new_names[i]
            i += 1
        data.rename(columns=columns, inplace=True)

        env_type=f"{self.params['env-detail']['arch-type']}"
        self.env_type=env_type
        self.log.info(f'Retrived environment type is {self.env_type}', exc_info=True)

        #Write the Pandas Data Frame into a CSV Format
        if self.env_type=="0":
            if self._get_platform()=="posixpath":                
                file_loc = self.params['storage-loc']['server-import-file-loc']
                file_name = f"{self.params['storage-loc']['server-import-file-loc']}/{self.import_file_name}"
            elif self._get_platform()=="windowspath":                
                file_loc = self.params['storage-loc']['server-import-file-loc']
                file_name = f"{self.params['storage-loc']['server-import-file-loc']}\{self.import_file_name}"
        else:
            if self._get_platform()=="posixpath":                            
                file_loc = self.params['storage-loc']['client-import-file-loc']
                file_name = f"{self.params['storage-loc']['client-import-file-loc']}/{self.import_file_name}"
            elif self._get_platform()=="windowspath":                
                file_loc = self.params['storage-loc']['client-import-file-loc']
                file_name = f"{self.params['storage-loc']['client-import-file-loc']}\{self.import_file_name}"

        #data.to_csv(file_name, index=False, quoting=1, quotechar='"')
        #If the time in data frame is already in epoch format then convert 
        #it to string format as siat supports string format only
        #if data[data.columns[0]].dtype == np.int64 or \
           #data[data.columns[0]].dtype == np.float64:
           #import pdb; pdb.set_trace()
           #data['time'] = pd.to_datetime(data.time, unit='ms').dt.strftime('%Y/%m/%d %H:%M:%S.%f')
           #str_time = '%Y/%m/%d %H:%M:%S.%f'
        try:
            data.to_csv(file_name, index=False)
        except:
            self.log.error(f'Unable to write to disk {file_name}, Check permissions', exc_info=True)
            raise IOError

    def _create_model(self, model_dict):
        r'''Creates a model and does the model id to name mapping

        Parameters
        ----------
        model_dict    :  dict
                         Json Dictionary which will be used to POST the request

        Returns
        -------
        1 : Successful creation of model
        0 : Failure while creating the model

        Notes
        -----
        **On the event of Model Creation failure check the logs to find
        the url used in post and also the http status code and
        SIAT response code which gets dumped into the logs.**

        '''
        self.log.info('Creating Model')
        utils = self.utils
        req_url = utils.build_url(key='model-url')

        task = 'ModelCreate'
        key = 'ModelID'
        mapping = self._get_model_mapping
        err = 'Model Creation Error'

        retval = self.post_and_get_async_result(req_url=req_url, task_dict=model_dict, task=task,\
                 mapping=mapping, key=key, err=err)

        if retval == 1:
            self.log.info("Successfully created Model")

        return retval

    def _get_model_mapping(self,model_id):
        r'''Creates a Model Name to Model ID mapping

        Parameters
        ----------
        model_id : str
                   Model ID as string

        Returns
        -------
        None

        '''
        self.model_mapping = {self.model_inputs["ModelName"] : model_id}

    def _create_model_dict(self, data):
        r'''This method is used internally to create the json dict
        needed to send a POST request for Model creation

        Parameters
        ----------
        data    : Pandas.DataFrame
                  The Data Frame from which StartTime and EndTime is calculated

        Raises
        ------
        AttributeError
            Raised if Metric Names are NOT registered

        Returns
        -------
        dict  :  dict
                 Returns a Dictionary Used as an input for Model Creation

        '''
        utils = self.utils
        model_inputs = self.model_inputs

        model_inputs["CreateUserName"] = self.params["server-details"]["createuser"]
        try:
            model_inputs["MetricID"] = list(self.metric_mapping.values())
        except:
            self.log.error("Register MetricGroups, Metrics, Import Metrics and then try creating Model", exc_info=True)
            raise
        model_inputs["MetricGroupID"] = list(self.group_mapping.values())

        model_inputs["MeasureInfoCount"] = 1
        self._populate_measure_info(data)

        if model_inputs.get("ModelName",None) == None:
            seconds = time.time()
            model_inputs["ModelName"] = f'Model.{int(seconds)}'

        current_time = utils.get_time_for_tzone(self.siat_tzone)
        
        if model_inputs.get("ModelComment",None) == None:
            model_inputs["ModelComment"] = f'Model {model_inputs["ModelName"]} created at {current_time}'

        if model_inputs.get("ModelType", None) == None:
            model_inputs["ModelType"] = 0

        if model_inputs.get("SignificantFigures",None) == None:
            model_inputs["SignificantFigures"] = 14

        if model_inputs.get("NegativeInvariantsHolds",None) == None:
            model_inputs["NegativeInvariantsHolds"] = 0

        if model_inputs.get("Severity", None) == None:
            model_inputs["Severity"] = 1

        if model_inputs.get("AccumalationID", None) == None:
            model_inputs["AccumalationID"] = 0

        if model_inputs.get("ExcludeAutoRegressive", None) == None:
            model_inputs["ExcludeAutoRegressive"] = 1

        if model_inputs.get("AutoRegressiveRange", None) == None:
            model_inputs["AutoRegressiveRange"] = 2

        if model_inputs.get("FitnessThreshold", None) == None:
            model_inputs["FitnessThreshold"] = 70

        if model_inputs.get("ModelCreateTiming", None) == None:
            model_inputs["ModelCreateTiming"] = 0

        return {"PlantMgrModelInfo" : model_inputs}

    def _populate_measure_info(self, data):
        r'''This method is used internally to populate the 
        MeasureInfo part of the json for POST request during
        model creation

        Parameters
        ----------
        data    :  Pandas.DataFrame
                   The Data Frame from which StartTime and EndTime is calculated

        Returns
        -------
        None
        '''
        #get the data from First row of the 'Time' Column
        #Assuming 'Time' is the first column
        utils = self.utils
        str_measure_info_start_time = data.iloc[0][0]
        tzone = self.siat_tzone
        #Take the index 1 of the returning tuple so that its the local server timezone

        #get the data from the last row of the 'Time Column'
        str_measure_info_end_time = data.iloc[-1][0]
        if data[data.columns[0]].dtype == np.int64 or \
           data[data.columns[0]].dtype == np.float64:
            measure_info_start_time = int(str_measure_info_start_time)
            measure_info_end_time = int(str_measure_info_end_time)
        else:
            measure_info_start_time = utils.convert_to_epoch_time(str_measure_info_start_time, tzone)[1]
            measure_info_end_time = utils.convert_to_epoch_time(str_measure_info_end_time, tzone)[1]

        self.log.debug(f'start-time-UTC: {str_measure_info_start_time}')
        self.log.debug(f'end-time-UTC: {str_measure_info_end_time}')

        dummylst = []
        i = 0
        while  i < self.model_inputs["MeasureInfoCount"]:
            dummylst.append({"StartTimeUTC": measure_info_start_time, "EndTimeUTC": measure_info_end_time})
            i += 1
        self.model_inputs["MeasureInfo"] = dummylst

    def fit(self, ThresholdEither, ThresholdBoth, MetricData,
            CalcParamN=2, CalcParamM=1, CalcParamK=0, **kwargs):
        r'''This method creates a baseline analysis to be able to be used to predict stuffs

        Its used to train the model and does the following stuffs sequentially

        1. Create Group and returns Group ID
        2. Create Metric and returns Metric ID.
        3. Import Metric Data (File) for training and returns result
        4. Create Model, Train the model and return the model id.

        Parameters
        ----------

        \*\*kwargs

        Below are the KeyWord Arguments that can be passed

        ThresholdEither    :   int
                               Fitness Threshold (either)
        ThresholdBoth      :   int
                               Fitness Threshold (both)
        CalcParamN         :   int, optional
                               Input Data Range  (N)
        CalcParamM         :   int, optional
                               Output Data Range (M)
        CalcParamK         :   int, optional
                               Time Range        (K)
        MetricData         :   `pandas.DataFrame`
                               Metrics Data Frame for import
        AnalysisInterval   :   unsigned int
                               Analysis Interval (milliseconds)

        Returns
        -------
        model_id   : On Success
        0          : On Failure

        '''
        self.log.info("Starting Fit to Train the Model")
        utils = self.utils
        self.kwargs = kwargs
        self.fit_data = MetricData.copy()
        self.fit_data[self.fit_data.columns[0]].dtype == np.int64
        AnalysisInterval = self._get_analysis_interval(data=self.fit_data)

        self.model_inputs = {}
        self.model_inputs['ThresholdEither']  = ThresholdEither
        self.model_inputs['ThresholdBoth']    = ThresholdBoth
        self.model_inputs['AnalysisInterval'] = AnalysisInterval
        #self.model_inputs['AnalysisInterval'] = 60000
        self.model_inputs['CalcParamN']       = CalcParamN
        self.model_inputs['CalcParamM']       = CalcParamM
        self.model_inputs['CalcParamK']       = CalcParamK

        #Create the Group and get the Group ID
        retval = self._register_metric_group()
        if retval == 1:
            #Create Metric and get the Metric ID
            retval = self._register_metrics_name()
            if retval == 1:
                #Import Metric Data File and return the import result
                retval = self._import_metric_file(self.fit_data, self.group_name)
                if retval == 1:
                    #Create Model, Train and return the model id
                    retval = self._create_model(model_dict=self._create_model_dict(self.fit_data))

        if retval == 1:
            self.log.info('Fit is Successfull')
        else:
            self.log.error('Fit Failed')
            return retval

        #Returns Model ID if successfull
        return list(self.model_mapping.values())[0]

    def predict(self, MetricData, ReturnType=3, **kwargs):
        r'''Does the Predict functionality of the SIAT

        Does the Following Tasks:

        1. Imports Metric Data for Prediction
        2. Creates On Demand Analysis.
        3. Returns Anomaly based on the return type

        Parameters
        ----------
        MetricData                : pandas.DataFrame
                                    Metric Data Frame for import
        ReturnType                : int, optional
                                    0 for Anomaly , 1 for BrokenInvariants, 2 for Top Sensors, 3 for All
        ModelID                   : int, optional
                                    ID created by fit() 
        ThresholdOverhead         : int, optional
                                    Detection threshold for broken invariants
        AnomalyType               : int, optional
                                    Anomaly score calculation type. Supports 1 or 0

        Returns
        -------
        loc : str
              Returns the CSV file location On Success
        0   : On Failure
        '''
        self.log.info("Preparing to Predict")
        kwargs['ReturnType'] = ReturnType
        self.predict_inputs = kwargs
        self.analyze_data = MetricData.copy()

        #Import Metric File

        if not self.group_name:
            #If fit is run then group_name attribute will be set else it will be empty
            group_name = self._fetch_group_name_for_model(kwargs['ModelID'])
        else :
            #Since fit is already run no need to fetch the group name
            group_name = self.group_name

        retval = self._import_metric_file(self.analyze_data, group_name)

        if retval == 1:
            #Request for analysis
            retval = self._request_for_analysis()
            if retval == 1:
                #Export Analysis Results
                retval = self._export_analysis_results()
                #Based on the Return Type return Anomaly
                if retval == 1:
                    self.analysis_results_loc = self._return_results_path()
                    if self.analysis_results_loc == 0:
                        retval = 0

        if retval == 1:
            self.log.info('Predict is Successful')
        else:
            self.log.error('Predict Failed')
            return 0

        return self.analysis_results_loc

    def _request_for_analysis(self):
        r'''This is used to send a POST request requesting for analysis of data

        Parameters
        ----------
        None

        Returns
        -------
        1 : On Success
        0 : On Failure
        '''
        self.log.info("Requesting for Analysis")
        utils = self.utils
        req_url = utils.build_url(key='analysis-url')

        self._prepare_for_analysis()
        analyze_dict = self.analyze_inputs

        task = 'ReqAnalysis'
        key = 'AnalysisID'
        mapping = self._get_analysis_mapping
        err = 'Request Analysis Error'

        retval = self.post_and_get_async_result(req_url=req_url, task_dict=analyze_dict, task=task,\
                 mapping=mapping, key=key, err=err)

        if retval == 1:
            self.log.info("Analysis Request Successful")

        return retval

    def _export_analysis_results(self):
        r'''Exports the analysis results

        Parameters
        ----------
        None

        Returns
        -------
        1 : On Success
        0 : On Failure
        '''
        self.log.info("Exporting Analysis Results")
        utils = self.utils
        req_id = list(self.analysis_mapping.values())[0]
        req_url = f"{utils.build_url(key='anomaly-export-url')}/{req_id}"
        #Store the Data in a Temp location

        file_loc = self.params["storage-loc"]["server-tmp-loc"]

        export_dict = {"PlantMgrAnalysisFilePath" : file_loc}
        task = 'ExportAnalysis'
        key = 'RequestID'
        mapping = self.log.debug
        err = 'Export Analysis Error'

        retval = self.post_and_get_async_result(req_url=req_url, task_dict=export_dict, task=task,\
                 mapping=mapping, key=key, err=err)

        if retval == 1:
            self.log.info("Successfully Exported Analysis Results")

        return retval

    def _return_results_path(self):
        r'''Returns the analysis results location

        Parameters
        ----------
        None

        Returns
        -------
        Tuple: On Success
        0    : On Failure
        '''
    #ADD CONDITIONAL STATEMENT BASED ON THE CLIENT OS
        env_type=f"{self.params['env-detail']['arch-type']}"
        self.env_type=env_type

        if self.env_type=="0":
            temp_loc = self.params['storage-loc']['server-tmp-loc']
            export_loc = self.params['storage-loc']['server-export-results']

            analysis_id = str(list(self.analysis_mapping.values())[0])

            anomaly_file = posixpath.join(temp_loc,analysis_id,f'{analysis_id}.csv')
            anomaly_dfile = posixpath.join(export_loc, f'{analysis_id}_anomaly.csv')

            broken_file = posixpath.join(temp_loc, analysis_id, f'{analysis_id}_constantly.csv')
            broken_dfile = posixpath.join(export_loc, f'{analysis_id}_broken_invariants.csv')

            ranking_file = posixpath.join(temp_loc, analysis_id, f'{analysis_id}_ranking.csv')
            ranking_dfile = posixpath.join(export_loc, f'{analysis_id}_top_sensors.csv')

        else:            
            temp_loc = self.params['storage-loc']['client-tmp-loc']
            export_loc = self.params['storage-loc']['client-export-results']

            analysis_id = str(list(self.analysis_mapping.values())[0])

            anomaly_file = os.path.join(temp_loc,analysis_id,f'{analysis_id}.csv')
            anomaly_dfile = os.path.join(export_loc, f'{analysis_id}_anomaly.csv')

            broken_file = os.path.join(temp_loc, analysis_id, f'{analysis_id}_constantly.csv')
            broken_dfile = os.path.join(export_loc, f'{analysis_id}_broken_invariants.csv')

            ranking_file = os.path.join(temp_loc, analysis_id, f'{analysis_id}_ranking.csv')
            ranking_dfile = os.path.join(export_loc, f'{analysis_id}_top_sensors.csv')

        #0 for Anomaly , 1 for BrokenInvariants, 2 for Top Sensors, 3 for All
        try:
            if self.predict_inputs['ReturnType'] == 0:
                if (os.path.exists(anomaly_file)):
                    shutil.copy(anomaly_file, anomaly_dfile)
                    return (anomaly_dfile,)
            elif self.predict_inputs['ReturnType'] == 1:
                if (os.path.exists(broken_file)):
                    shutil.copy(broken_file, broken_dfile)
                    return (broken_dfile,)
            elif self.predict_inputs['ReturnType'] == 2:
                if (os.path.exists(ranking_file)):
                    shutil.copy(ranking_file, ranking_dfile)
                    return (ranking_dfile,)
            elif self.predict_inputs['ReturnType'] == 3:
                if ((os.path.exists(anomaly_file))    and
                   (os.path.exists(broken_file))  and
                   (os.path.exists(ranking_file))):
                    shutil.copy(anomaly_file, anomaly_dfile)
                    shutil.copy(broken_file, broken_dfile)
                    shutil.copy(ranking_file, ranking_dfile)
                    return (anomaly_dfile, broken_dfile, ranking_dfile,)
            else:
                log.error("Wrong ReturnType")
                return 0
        except:
            self.log.error("Error when copying results", exc_info=True)
            return 0
        return 0

    def _get_platform(self):
        r'''Used to get the OS platform of installed SIAT wrapper

        Parameters
        ----------
        None

        Returns
        -------
        windowspath :   str
                        Incase 'win32' received for Windows environment
        
        posixpath   :   str
                        Other than Windows OS.
        '''

        if sys.platform.startswith('win32'):
            os_platform='windowspath'
        else:
            os_platform='posixpath'
        return os_platform

    def _get_analysis_mapping(self, analysis_id):
        r'''Used to create a mapping of analysis name and analysis id

        Parameters
        ----------

        analysis_id  : str
                       Analysis ID as string

        Returns
        -------
        None
        '''
        self.analysis_mapping = {self.analyze_inputs["AnalysisName"] : analysis_id}

    def _prepare_for_analysis(self):
        r'''Used to create a json input for POST request to analyze data

        Reads the predict_inputs dict and creates an analyze_inputs dict
        which will be used POST the analysis request to SIAT

        Parameters 
        ----------
        None

        Returns
        -------
        None
        '''
        predict_inputs = self.predict_inputs
        analyze_inputs = {}
        plant_mgr_analyze_req = {}

        if predict_inputs.get("AnalysisName",None) == None:
            seconds = time.time()
            analyze_inputs["AnalysisName"] = f'Analysis.{int(seconds)}'

        if predict_inputs.get("AnalyzeType",None) == None:
            analyze_inputs["AnalyzeType"] = 0

        if predict_inputs.get("ModelCount", None) == None:
            analyze_inputs["ModelCount"] = 1

        if predict_inputs.get("UserName",None) == None:
            analyze_inputs["UserName"] = self.params["server-details"]["createuser"]

        if predict_inputs.get("ModelID",None) == None:
            #Use the id of the model created by fit()
            model_id = list(self.model_mapping.values())[0]
            analyze_inputs["PlantMgrModelScheduleInfo"] = [{"ModelID" : model_id}]
        else:
            model_id = predict_inputs["ModelID"]
            analyze_inputs["PlantMgrModelScheduleInfo"] = [{"ModelID" : model_id}]

        if predict_inputs.get("ThresholdOverhead", None) == None:
           plant_mgr_analyze_req["ThresholdOverhead"] = 1.1
        else:
           plant_mgr_analyze_req["ThresholdOverhead"] =  predict_inputs["ThresholdOverhead"]

        if predict_inputs.get("AnomalyType", None) == None:
            plant_mgr_analyze_req["AnomalyType"] = 1
        else:
            plant_mgr_analyze_req["AnomalyType"] = predict_inputs["AnomalyType"]

        if predict_inputs.get("Detection", None) == None:
            plant_mgr_analyze_req["Detection"] = 980000
        else:
            plant_mgr_analyze_req["Detection"] = predict_inputs["Detection"]

        if predict_inputs.get("Recovery", None) == None:
            plant_mgr_analyze_req["Recovery"] = 500000
        else:
            plant_mgr_analyze_req["Recovery"] = predict_inputs["Recovery"]

        if predict_inputs.get("ControlSpan", None) == None:
            plant_mgr_analyze_req["ControlSpan"] = 10000
        else:
            plant_mgr_analyze_req["ControlSpan"] = predict_inputs["ControlSpan"]

        if predict_inputs.get("AccumalationID", None) == None:
            plant_mgr_analyze_req["AccumalationID"] = 0
        else:
            plant_mgr_analyze_req["AccumalationID"] = predict_inputs["AccumalationID"]

        if predict_inputs.get("ExcludeConstantlyBroken", None) == None:
            plant_mgr_analyze_req["ExcludeConstantlyBroken"] = 1
        else:
            plant_mgr_analyze_req["ExcludeConstantlyBroken"] = predict_inputs["ExcludeConstantlyBroken"]

        if predict_inputs.get("SamplingPoints", None) == None:
            plant_mgr_analyze_req["SamplingPoints"] = 100
        else:
            plant_mgr_analyze_req["SamplingPoints"] = predict_inputs["SamplingPoints"]

        if predict_inputs.get("ContinuousPeriod", None) == None:
            AnalysisInterval = self._get_analysis_interval(data=self.analyze_data)
            plant_mgr_analyze_req["ContinuousPeriod"] = plant_mgr_analyze_req["SamplingPoints"] * AnalysisInterval
        else:
            plant_mgr_analyze_req["ContinuousPeriod"] = predict_inputs["ContinuousPeriod"]

        if predict_inputs.get("ThresholdConstantlyBroken", None) == None:
            plant_mgr_analyze_req["ThresholdConstantlyBroken"] = 700000
        else:
             plant_mgr_analyze_req["ThresholdConstantlyBroken"] =  predict_inputs["ThresholdConstantlyBroken"]

        if predict_inputs.get("ExcessMonitoringTime", None) == None:
            plant_mgr_analyze_req["ExcessMonitoringTime"] = 0
        else:
            plant_mgr_analyze_req["ExcessMonitoringTime"] = predict_inputs["ExcessMonitoringTime"]

        if predict_inputs.get("ExcessRate", None) == None:
            plant_mgr_analyze_req["ExcessRate"] = 100
        else:
            plant_mgr_analyze_req["ExcessRate"] = predict_inputs["ExcessRate"]

        analyzetime = self._populate_start_end_time()
        plant_mgr_analyze_req["StartTime"] = analyzetime[0]
        plant_mgr_analyze_req["EndTime"] = analyzetime[1]

        analyze_inputs["PlantMgrAnalyzeReq"] =  plant_mgr_analyze_req
        self.analyze_inputs = analyze_inputs

    def _populate_start_end_time(self):
        r'''Gets the start and end time from the pandas DataFrame

        Parameters
        ----------
        None

        Returns
        -------
        (start_time, end_time)   :  tuple
                                    Returns a Tuple with start and end time as the members
        '''
        #get the data from First row of the 'Time' Column
        #Assuming 'Time' is the first column
        data = self.analyze_data
        utils = self.utils
        str_measure_info_start_time = data.iloc[0][0]
        self.log.debug(f'start-time-UTC: {str_measure_info_start_time}')
        tzone = self.siat_tzone
        #Take the index 1 of the returning tuple so that its the local server timezone
        #get the data from the last row of the 'Time Column'
        str_measure_info_end_time = data.iloc[-1][0]
        self.log.debug(f'end-time-UTC: {str_measure_info_end_time}')

        if data[data.columns[0]].dtype == np.int64 or \
           data[data.columns[0]].dtype == np.float64:
            #Epoch Format
            measure_info_start_time = int(str_measure_info_start_time)
            measure_info_end_time = int(str_measure_info_end_time)
        else:
            measure_info_start_time = utils.convert_to_epoch_time(str_measure_info_start_time, tzone)[1]
            measure_info_end_time = utils.convert_to_epoch_time(str_measure_info_end_time, tzone)[1]

        return (measure_info_start_time,  measure_info_end_time)

    def _get_analysis_interval(self, data):
        utils = self.utils
        str_start_time = data.iloc[0][0]
        tzone = self.siat_tzone
        str_end_time = data.iloc[1][0]
        #Check if Time is in String or epoch format
        if data[data.columns[0]].dtype == np.int64 or \
           data[data.columns[0]].dtype == np.float64:
            #Epoch Format
            start_time = str_start_time
            end_time = str_end_time
        else:
            #String Format, so converting it to EPOCH
            start_time = utils.convert_to_epoch_time(str_start_time, tzone)[1]
            end_time = utils.convert_to_epoch_time(str_end_time, tzone)[1]

        analysis_interval = end_time - start_time
        return int(analysis_interval)


    def post_and_get_async_result(self, req_url, task_dict, task, mapping, key, err):
        r'''This method posts a request which is async in nature and loops till it gets
        the expected response.

        Sends a Post request and does subsequent get request to see if its 200 OK.
        It loops till we get a 200 OK and once it gets a 200 OK and result is
        as expected it does a mapping of the request id and name.

        Parameters
        ----------
        req_url      :   str
                         URL for POST request

        task_dict    :   dict
                         Input JSON for POST Request

        task         :   str
                         Specify What Task you are doing in POST

        mapping      :   func
                         Pass a function that you want to execute to get the mapping
                         in the event of success.

        key          :   str
                         Specify the key to look for in the Response Code

        err          :   str
                         Specify the custom error message that you want to print on the
                         event of a failure

        Returns
        -------
        1    :   On Success
        0    :   On Failure
        '''
        utils = self.utils
        res = utils.send_request(url=req_url, req_type='post', post_dict=task_dict)

        retval = utils.check_http_status_code(res, requests.codes.accepted)

        if (retval == 1):
            retval = utils.check_response_code(response=res, req_type='async', expected_res=1, key=key)
            request_id = utils.idtocheck
            #time.sleep(1)
            retval = utils.wait_for_request_to_complete(request_id, task=task)

            if retval == 1:
                #Do an id to name mapping
                mapping(request_id)
        else:
            utils._lookup_response(res, req_type = task, http_type = '202')

        return retval

    def _get_group_id_from_model_id(self, model_id):
        r'''This method does a get request to get the group id from model id

        Parameters
        ----------
        model_id     :   int
                         ModelID as passed by the user.

        Returns
        -------
        group_id     :   On Success
        0            :   On Failure
        '''
        self.log.info("Fetching GroupID associated with the Model")
        utils = self.utils
        req_url = utils.build_url(key='model-url')
        req_url = f'{req_url}/{model_id}'
        res = utils.send_request(url=req_url, req_type='get')

        retval = utils.check_http_status_code(res,requests.codes.ok)

        if (retval == 1):
            #Correct HTTP Status Code
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            if(retval == 1):
                #Correct SIAT Response Code
                try:
                    group_id = res.json()['PlantMgrModelInfo']['MetricGroupID'][0]
                except:
                    self.log.error("Error when checking response", exc_info=True)
                    return 0
                self.log.info("Metric GroupID mapping done successfully")
            else:
                #Wrong Response Code from SIAT
                self.log.error(f'Unable to retrieve Group ID for ModelID {model_id}')
                return 0
                self.log.error(res.json())
        else:
            #Wrong HTTP Status Code
            return 0
            self.log.error(f'Unable to retrieve Model Info for ModelID {model_id}')
            self.log.error(res.text)
        return group_id

    def _get_group_name_from_group_id(self, group_id):
        r'''This method does a get request to get the group name from group id

        Parameters
        ----------
        group_id     :   str
                         Group id of the model

        Returns
        -------
        group_name   :   On Success
        0            :   On Failure
        '''
        self.log.info("Fetching GroupName from GroupID")
        #Filtering with GroupID is NOT working so using workaround to 
        #get the Group Name. 
        utils = self.utils
        req_url = utils.build_url(key='metric-url')

        req_url = f'{req_url}?mode=1&groupid={group_id}'
        res = utils.send_request(url=req_url, req_type='get')

        retval = utils.check_http_status_code(res,requests.codes.ok)

        if (retval == 1):
            #Correct HTTP Status Code
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            if(retval == 1):
                #Correct SIAT Response Code
                try:
                    metric_name = res.json()['PlantMgrMetricInfo'][0]['MetricName']
                    #The Metric Name will have the Group Name prepended to it during fit.
                    #Using that to retrieve the Group Name
                    group_name = metric_name.split('.')[0]
                except:
                    self.log.error("Error when checking response", exc_info=True)
                    return 0
                self.log.info("Metric GroupID mapping done successfully")
            else:
                #Wrong Response Code from SIAT
                self.log.error(f'Unable to retrieve GroupName for GroupID {group_id}')
                return 0
                self.log.error(res.json())
        else:
            #Wrong HTTP Status Code
            return 0
            self.log.error(f'Unable to retrieve Metrics for GroupID {group_id}')
            self.log.error(res.text)
        return group_name

    def _fetch_group_name_for_model(self, model_id):
        '''Wrapper to get group name for a model
        '''
        group_id = self._get_group_id_from_model_id(model_id)
        group_name = self._get_group_name_from_group_id(group_id)
        return group_name

    def _get_model_info_to_clone(self, model_id):
        r'''This method does a get request to get the Model Info of a Model

        Parameters
        ----------
        model_id     :   int
                         Model id of the model

        Returns
        -------
        model_info   :   On Success
        0            :   On Failure
        '''
        self.log.info(f'Fetching ModelInfo associated with the ModelID {model_id}')
        utils = self.utils
        req_url = utils.build_url(key='model-url')
        req_url = f'{req_url}/{model_id}'
        res = utils.send_request(url=req_url, req_type='get')

        retval = utils.check_http_status_code(res,requests.codes.ok)

        if (retval == 1):
            #Correct HTTP Status Code
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            if(retval == 1):
                #Correct SIAT Response Code
                try:
                    model_info = res.json()
                except:
                    self.log.error("Error when checking response", exc_info=True)
                    return 0
                self.log.info("Retrieved the ModelInfo successfully")
            else:
                #Wrong Response Code from SIAT
                self.log.error(f'Unable to retrieve ModelInfo for ModelID {model_id}')
                return 0
                self.log.error(res.json())
        else:
            #Wrong HTTP Status Code
            return 0
            self.log.error(f'Unable to retrieve ModelInfo for ModelID {model_id}')
            self.log.error(res.text)
        return model_info

    def _update_model_inputs(self, model_response, ThresholdEither,
            ThresholdBoth, AnalysisInterval, CalcParamN,
            CalcParamM,  CalcParamK):
        r'''This method creates a dict by cloning from an existing Model

        Here we create the dict which will be used to create a model. The
        dict is populated from an existing Model model_response.

        Parameters
        ----------
        model_response     :   dict
                               Response from SIAT which contains ModelInfo
        ThresholdEither    :   int
                               Fitness Threshold (either)
        ThresholdBoth      :   int
                               Fitness Threshold (both)
        AnalysisInterval   :   unsigned int
                               Analysis Interval (milliseconds)
        CalcParamN         :   int, optional
                               Input Data Range  (N)
        CalcParamM         :   int, optional
                               Output Data Range (M)
        CalcParamK         :   int, optional
                               Time Range        (K)
                               
        Returns
        -------
        final_dict   :   On Success
        0            :   On Failure
        '''
        keys = ["CreateUserName", "MetricID", "MetricGroupID", 
                "MeasureInfoCount", "ModelName", "ModelComment",
                "ModelType", "SignificantFigures", "NegativeInvariantsHolds",
                "Severity", "AccumulationID" , "ExcludeAutoRegressive" ,
                "AutoRegressiveRange", "FitnessThreshold", "ModelCreateTiming"]

        model_inputs = {}
        utils = self.utils
        #Cloning the Model Info from the existing model
        try:
            for x in keys:
                if x == "ModelName":
                    old_name = model_response['PlantMgrModelInfo'][x]
                    new_name = f'{old_name}.{int(time.time())}'
                    model_inputs[x] = new_name
                elif x == "ModelComment":
                    old_comment = model_response['PlantMgrModelInfo']['ModelComment']
                    current_time = utils.get_time_for_tzone(self.siat_tzone)
                    model_comment = f'Updated at {current_time}'
                    model_comment += f' based on the {old_comment}'
                    model_inputs[x] = model_comment
                else:
                    model_inputs[x] = model_response['PlantMgrModelInfo'][x]

            start_time = model_response['PlantMgrModelInfo']['MeasureInfo'][0]['StartTimeUTC']
            end_time = model_response['PlantMgrModelInfo']['MeasureInfo'][0]['EndTimeUTC']
            measure_info = [{"StartTimeUTC": start_time, "EndTimeUTC": end_time}]
        except:
            self.log.error("Looks like Response is empty", exc_info=True)
            return 0

        model_inputs["MeasureInfo"] = measure_info

        #Inputs from User
        model_inputs['ThresholdEither']  = ThresholdEither
        model_inputs['ThresholdBoth']    = ThresholdBoth
        model_inputs['AnalysisInterval'] = AnalysisInterval
        model_inputs['CalcParamN']       = CalcParamN
        model_inputs['CalcParamM']       = CalcParamM
        model_inputs['CalcParamK']       = CalcParamK

        final_dict = { 'PlantMgrModelInfo': model_inputs}

        return final_dict

    def update_model(self,ThresholdEither, ThresholdBoth, AnalysisInterval,
             CalcParamN,  CalcParamM,  CalcParamK, ModelID):
        r'''This method is used to update a Model

        Parameters
        ----------
        ThresholdEither    :   int
                               Fitness Threshold (either)
        ThresholdBoth      :   int
                               Fitness Threshold (both)
        AnalysisInterval   :   unsigned int
                               Analysis Interval (milliseconds)
        CalcParamN         :   int, optional
                               Input Data Range  (N)
        CalcParamM         :   int, optional
                               Output Data Range (M)
        CalcParamK         :   int, optional
                               Time Range        (K)
        ModelID            :   int
                               Model id of the model

        Returns
        -------
        model_id     :   On Success
        0            :   On Failure
        '''
        self.log.info("Updating Model")

        res = self._get_model_info_to_clone(ModelID)

        if res != 0:
            model_dict = self._update_model_inputs(res, ThresholdEither,
                    ThresholdBoth, AnalysisInterval, CalcParamN,
                    CalcParamM,  CalcParamK)                 

            if model_dict != 0:
                retval = self._create_model(model_dict=model_dict)
            else:
                retval = 0
        else:
            retval = 0

        if retval == 1:
            self.log.info('Model Update Successful')
        else:
            self.log.error('Model Update Failed')
            return 0

        return list(self.model_mapping.values())[0]


    def model_export(self, ModelID):
        ''' Takes ModelID, uses REST call to export the Model data
        as a csv file

        Parameters
        ----------
        ModelID    :    int
                        Model ID of the Model which needs to be exported

        Returns
        -------
        loc         :   str
                        Returns the absolute path of the exported csv file
        0           :   int
                        if fails to export
        '''
        utils = self.utils
        req_url = f"{utils.build_url(key='model-export-url')}/{ModelID}"
        file_loc = self.params['storage-loc']['server-tmp-loc']
        self.export_path = {"PlantMgrModelFilePath" : file_loc}

        self.log.info(f'Exporting model for the ModedID: {ModelID}')

        task = 'ExportModel'
        key = 'RequestID'
        mapping = self.log.debug
        err = 'Export Model Error'

        retval = self.post_and_get_async_result(req_url=req_url,
                 task_dict=self.export_path, task=task,
                 mapping=mapping, key=key, err=err)

        # os_type=self.get_platform()
        # self.log.info(f'OS version successfully captured!!! OS of the server is: {os_type}')

        if retval == 1:
            self.log.info(f'Model: {ModelID} exported successfully, starting model coversion...')
            retval = self._export_conversion(ModelID)
        else:
            self.log.error(f'Model: {ModelID} export failed!')    

        return retval
    


    def _export_conversion(self, model_id):
        ''' Called by Model export method and converts exported mdlfile to csv

        Parameters
        ----------
        model_id    : passed through modelexport method


        Returns
        -------
        file_loc    : str
                      location of converted file if successfully converted
        0           : int
                      if conversion failed
        '''
        utils = self.utils
        env_type=f"{self.params['env-detail']['arch-type']}"
        self.env_type=env_type
        if self.env_type=="0":            
            temp_file_loc = self.params['storage-loc']['server-tmp-loc']
            #The Location where the converted csv file needs to be placed
            file_loc =  self.params['storage-loc']['server-export-results']
        else:            
            temp_file_loc = self.params['storage-loc']['client-tmp-loc']
            #The Location where the converted csv file needs to be placed
            file_loc =  self.params['storage-loc']['client-export-results']
 
        #Location of the MDL file which needs to be converted
        mdl_file_path = os.path.join(temp_file_loc, f'{str(model_id)}')
        mdl_csv_file  = os.path.join(mdl_file_path, f'{str(model_id)}.csv')
        if (os.path.exists(mdl_csv_file)):
            self.log.info(f'Exported {model_id} folder and csv file'
                          f' found at {mdl_file_path}. Starting mdl to csv conversion')
        else:
            self.log.error(f'Could Not find {mdl_csv_file}')
            return 0

        out_csv = os.path.join(mdl_file_path, f'{str(model_id)}_Model.csv')
        export_loc = os.path.join(file_loc, f'{str(model_id)}_Model.csv')
        utils.mdl_to_csv(model_id = model_id, base_dir=temp_file_loc)
        self.log.info(f'Method to convert mdl to csv is successful')
        if (os.path.exists(out_csv)):
            self.log.info(f'Converted from MDL to CSV')
            shutil.copy(out_csv, export_loc)
            self.log.info(f'File Absolute Path : {export_loc}')
            return export_loc
        else:
            self.log.error(f'Convertion failed from MDL to CSV')
            return 0

    def model_import(self, inFile):
        ''' Takes csv file path as i/p parameter.
            uses '_import_conversion' method to convert to mdl
            and REST call to import converted mdl and create a model

        Parameters
        ----------
        inFile      :   str
                        input csv file with updated changes in the relationship
        
        Returns
        -------
        model_id    : int
                      Model ID of the newly imported model On Success

        0           : int
                      On Failure

        '''
        utils = self.utils 
        req_url = f"{utils.build_url(key='model-import-url')}"
        self.log.info(f'Importing model from csv file {inFile}')
        retval = self._import_conversion(inFile)

        if (retval == 1):
            self.log.info(f'import conversion successful.'
                           'Initiating REST Call')

        else:
            self.log.error(f'Model import failed!')
            return 0


        #Imported Server-side import file location
        env_type=f"{self.params['env-detail']['arch-type']}"
        self.env_type=env_type
        if self.env_type=="0": 
            server_import_path= f"{self.params['storage-loc']['server-import-file-loc']}"
        else:
            server_import_path= f"{self.params['storage-loc']['server-tmp-loc']}"            

        self.server_import_path=server_import_path

        temp_model_id = os.path.basename(self.import_file_path)
        server_import_file_path=posixpath.join(self.server_import_path,temp_model_id)
        self.server_import_file_path=server_import_file_path
        import_path = {"PlantMgrModelFilePath": f'{self.server_import_file_path}/{temp_model_id}.csv'}        
        self.log.info('Importing model ... '
                      f'Parameter passed in Import API call is :{import_path}')

        task = 'ImportModel'
        key = 'RequestID'
        mapping = self.log.debug
        err = 'Import Model Error'
        retval = self.post_and_get_async_result(req_url=req_url, task_dict=import_path, task=task,\
                 mapping=mapping, key=key, err=err)

        if retval == 1:
            self.log.info(f"Successfully Imported Model")
        else:
            self.log.error(f"Unable to Import Model")
            return 0

        retval = self._get_model_id_from_model_name(utils.model_name)
 
        return retval

    def _import_conversion(self, imp_csv_file):
        ''' Called by Modelimport method to convert the 
        csvfile->binary/mdlfile

        Parameters
        ----------
        inFile      :    Passed through modelimport method
        
        Returns
        -------
        1           : int
                      if Successful
        0           : int
                      On Failure
        '''
        utils = self.utils
        if self.env_type=="0":     
            import_dir = self.params['storage-loc']['server-import-file-loc']
        else:
            import_dir = self.params['storage-loc']['client-tmp-loc']            

        if (os.path.exists(imp_csv_file)):
            self.log.info(f'csv file found at : {imp_csv_file}.'
                          f' Starting MDL conversion for import model')
        else:
            self.log.error(f"Looks like csv file is not present at : {imp_csv_file}"
                             f" for import process")
            return 0

        import_file_path =  utils.csv_to_mdl(imp_csv_file, import_dir, self.siat_tzone)
        temp_model_id = os.path.basename(import_file_path)
        data_file = os.path.join(import_file_path,f'{temp_model_id}_data.dat')
        def_file = os.path.join(import_file_path,f'{temp_model_id}_def.dat')
        csv_file = os.path.join(import_file_path,f'{temp_model_id}.csv')

        if (os.path.exists(data_file) == True and os.path.exists(def_file) == True
                and os.path.exists(csv_file) == True):
            self.log.info(f'CSV to MDL conversion successful. MDL file at: {import_file_path}.' 
                           ' Initiating import call.')
        else:
            self.log.error(f'Converted .dat files for import not present at: {import_file_path},'
                            ' CSV to MDL  process may have failed!')
            return 0
        
        self.import_file_path = import_file_path
        return 1

    def _get_model_id_from_model_name(self, model_name):
        r'''This method does a get request to get the model id from model name

        Parameters
        ----------
        model_name   :   str
                         model_name as passed by the user.

        Returns
        -------
        model_id     :   On Success
        0            :   On Failure
        '''
        self.log.info(f"Fetching ModelID associated with the Model {model_name}")
        utils = self.utils
        req_url = utils.build_url(key='model-url')
        req_url = f'{req_url}?modelname={model_name}'
        res = utils.send_request(url=req_url, req_type='get')

        retval = utils.check_http_status_code(res,requests.codes.ok)

        if (retval == 1):
            #Correct HTTP Status Code
            retval = utils.check_response_code(response=res, req_type='sync', expected_res=1)
            if(retval == 1):
                #Correct SIAT Response Code
                try:
                    model_id = res.json()['PlantMgrModelList'][0]['ModelID']
                except:
                    self.log.error("Error when checking response", exc_info=True)
                    return 0
                self.log.info("ModelID mapping done successfully")
            else:
                #Wrong Response Code from SIAT
                self.log.error(f'Unable to retrieve Model ID for ModelName {model_name}')
                return 0
                self.log.error(res.json())
        else:
            #Wrong HTTP Status Code
            return 0
            self.log.error(f'Unable to retrieve ModelID for ModelName {model_name}')
            self.log.error(res.text)
        return model_id


if __name__ == '__main__':
    from siatlib import siatlogging
    logs = siatlogging.SiatLogging(filename='restdebuglogs', loglevel=logging.DEBUG)
