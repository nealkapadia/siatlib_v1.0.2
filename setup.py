import setuptools

with open("README.md", "r") as fh:
        long_description = fh.read()
        setuptools.setup(
                name='siatlib',
                version='1.0.2',
                scripts=['scripts/siat.py','scripts/siatutils.py','scripts/siatlogging.py','scripts/__init__.py'],
                author="NECAM",
                author_email="siat_iag@necaa.jp.nec.com",
                description="A python wrapper package of SIAT engine",
                packages=["siatlib"],
                classifiers=[
                    "Programming Language :: Python :: 3",
                    "License :: OSI Approved :: MIT License",
                    "Operating System :: OS Independent",
                ],
)